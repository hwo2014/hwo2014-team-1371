//
// Track.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;

namespace Data
{
	[Serializable]
    public class Track
    {
		public enum LaneSwitch
		{
			None,
			Left,
			Right
		}
		
		[Serializable]
		public class Piece
		{
			public double length;
			public bool @switch;
			public double radius;
			public double angle;

			public TrackTransform startPoint;
			public TrackTransform transform;
			public double lapProgressStart;
			public double lapProgressEnd;
		}

		[Serializable]
		public class Lane
		{
			public double distanceFromCenter;
			public int index;
		}

		public string id;
		public string name;
		public Piece[] pieces;
		public Lane[] lanes;
		public TrackTransform startingPoint;
		public double lapLength;

		public TrackGraph trackGraph = new TrackGraph();

		public bool Built = false;

		public void Build()
		{
			if (Built)
			   return;
			lapLength = 0;
			TrackTransform transform = startingPoint;
			foreach (var piece in pieces)
			{
				piece.startPoint = transform;
				Position offset = new Position(piece.radius * Math.Sign(piece.angle), 0);
				piece.transform = new TrackTransform(new Position(0, piece.length) + offset - offset.Rotated(piece.angle), piece.angle);
				transform = transform * piece.transform;
				double pieceLength = piece.length + piece.radius * Math.PI * 2 * Math.Abs(piece.angle) / 360;
				piece.lapProgressStart = lapLength;
				lapLength += pieceLength;
				piece.lapProgressEnd = lapLength;
			}
			foreach (var piece in pieces)
			{
				piece.lapProgressStart /= lapLength;
				piece.lapProgressEnd /= lapLength;
			}
			Built = true;
			trackGraph.Build(this);
		}
		
		public CarPosition.PiecePosition LookAhead(CarPosition.PiecePosition position, double distance, LaneSwitch laneSwitch)
		{
			var newPiecePosition = new CarPosition.PiecePosition(position);

			// TODO: Return all combinations of lane switches
			while (distance > 0)
			{
				double pieceLength = GetPieceLengthAtPosition(newPiecePosition);
				if (distance < pieceLength - newPiecePosition.inPieceDistance)
				{
					newPiecePosition.inPieceDistance += distance;
					break;
				}
				distance -= pieceLength - newPiecePosition.inPieceDistance;
				newPiecePosition.pieceIndex = (newPiecePosition.pieceIndex + 1) % pieces.Length;
				newPiecePosition.lane.startLaneIndex = newPiecePosition.lane.endLaneIndex;
				switch (laneSwitch)
				{
					case LaneSwitch.None:
						break;
					case LaneSwitch.Left:
						newPiecePosition.lane.endLaneIndex = Math.Max(newPiecePosition.lane.startLaneIndex - 1, 0);
						break;
					case LaneSwitch.Right:
						newPiecePosition.lane.endLaneIndex = Math.Min(newPiecePosition.lane.startLaneIndex + 1, lanes.Length - 1);
						break;
				}
				laneSwitch = LaneSwitch.None;
				newPiecePosition.inPieceDistance = 0;
			}

			return newPiecePosition;
		}

		public TrackGraph.Sample GetClosestSample(CarPosition carPosition)
		{
			return GetClosestSample(carPosition.piecePosition);
		}
		
		public TrackGraph.Sample GetClosestSample(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();
			
			return trackGraph.GetClosestSample(this, piecePosition);
		}

		public TrackGraph.Edge GetCurrentEdge(CarPosition carPosition)
		{
			return GetCurrentEdge(carPosition.piecePosition);
		}

		public TrackGraph.Edge GetCurrentEdge(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();

			return trackGraph.GetCurrentEdge(this, piecePosition);
		}

		public double GetPieceTorque(CarPosition carPosition)
		{
			return GetPieceTorque(carPosition.piecePosition);
		}
		
		public double GetPieceTorque(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();

			var piece =  pieces[piecePosition.pieceIndex];
			var pieceRadiusAtPosition = GetPieceRadiusAtPosition(piecePosition);
			var pieceLengthAtRadius = piece.radius > 0 ? Math.Abs(piece.angle) / 360 * 2 * Math.PI * pieceRadiusAtPosition : 0;
			return pieceLengthAtRadius > 0 ? TrackTransform.FixAngle(piece.angle / pieceLengthAtRadius) : 0;
		}

		public double GetPieceProgress(CarPosition carPosition)
		{
			return GetPieceProgress(carPosition.piecePosition);
		}

		public double GetPieceProgress(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();

			double estimatedLength = GetPieceLengthAtPosition(piecePosition);

			// TODO: Check for error when changing pieces
			double estimatedProgress = piecePosition.inPieceDistance / estimatedLength;
			return estimatedProgress;
		}

		public double GetLapProgress(CarPosition carPosition)
		{
			return GetLapProgress(carPosition.piecePosition);
		}

		public double GetLapProgress(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();

			double pieceProgress = GetPieceProgress(piecePosition);
			var piece = pieces[piecePosition.pieceIndex];
			return piece.lapProgressStart + (piece.lapProgressEnd - piece.lapProgressStart) * pieceProgress;
		}

		public double GetPieceLengthAtPosition(CarPosition carPosition)
		{
			return GetPieceLengthAtPosition(carPosition.piecePosition);
		}
		
		public double GetPieceLengthAtPosition(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();
			
			var piece = pieces[piecePosition.pieceIndex];
			var firstLane = lanes[piecePosition.lane.startLaneIndex];
			var lastLane = lanes[piecePosition.lane.endLaneIndex];
			
			double firstLaneRadius = (piece.radius + (firstLane.distanceFromCenter) * Math.Sign(-piece.angle));
			double lastLaneRadius = (piece.radius + (lastLane.distanceFromCenter) * Math.Sign(-piece.angle));
			double firstLaneLength = piece.length + Math.Abs(piece.angle) / 360 * 2 * Math.PI * firstLaneRadius;
			double lastLaneLength = piece.length + Math.Abs(piece.angle) / 360 * 2 * Math.PI * lastLaneRadius;
			
			double estimatedLength = (firstLaneLength + lastLaneLength) * 0.5;

			// Assume switches are rectangles
			if (piecePosition.lane.startLaneIndex != piecePosition.lane.endLaneIndex)
				estimatedLength = Math.Pow(Math.Pow(estimatedLength, 2) + Math.Pow(firstLane.distanceFromCenter - lastLane.distanceFromCenter, 2), 0.5);

			// TODO: Better approximation
			return estimatedLength;
		}
		
		public double GetPieceRadiusAtPosition(CarPosition carPosition)
		{
			return GetPieceRadiusAtPosition(carPosition.piecePosition);
		}

		public double GetPieceRadiusAtPosition(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();
			
			var piece = pieces[piecePosition.pieceIndex];
			var firstLane = lanes[piecePosition.lane.startLaneIndex];
			var lastLane = lanes[piecePosition.lane.endLaneIndex];

			double firstLaneRadius = (piece.radius + (firstLane.distanceFromCenter) * Math.Sign(-piece.angle));
			double lastLaneRadius = (piece.radius + (lastLane.distanceFromCenter) * Math.Sign(-piece.angle));

			// TODO: Check for error when changing pieces
			double estimatedProgress = GetPieceProgress(piecePosition);

			return firstLaneRadius * estimatedProgress + lastLaneRadius * (1 - estimatedProgress);
		}

		public TrackTransform GetTrackTransform(CarPosition carPosition)
		{
			return GetTrackTransform(carPosition.piecePosition);
		}
			
		public TrackTransform GetTrackTransform(CarPosition.PiecePosition piecePosition)
		{
			if (!Built)
				Build();

			var piece = pieces[piecePosition.pieceIndex];
			var firstLane = lanes[piecePosition.lane.startLaneIndex];
			var lastLane = lanes[piecePosition.lane.endLaneIndex];
			TrackTransform pieceStart = piece.startPoint;

			double firstLaneRadius = (piece.radius + (firstLane.distanceFromCenter) * Math.Sign(-piece.angle));
			double lastLaneRadius = (piece.radius + (lastLane.distanceFromCenter) * Math.Sign(-piece.angle));

			// TODO: Check for error when changing pieces
			double estimatedProgress = GetPieceProgress(piecePosition);

			Position firstLaneOffset = new Position(firstLaneRadius * Math.Sign(piece.angle), 0);
			TrackTransform firstLanePosition = pieceStart * new TrackTransform(new Position(firstLane.distanceFromCenter, piece.length * estimatedProgress)
			                                                                   + firstLaneOffset - firstLaneOffset.Rotated(piece.angle * estimatedProgress),
			                                                                   piece.angle * estimatedProgress);
			Position lastLaneOffset = new Position(lastLaneRadius * Math.Sign(piece.angle), 0);
			TrackTransform lastLanePosition = pieceStart * new TrackTransform(new Position(lastLane.distanceFromCenter, piece.length * estimatedProgress)
			                                                                  + lastLaneOffset - lastLaneOffset.Rotated(piece.angle * estimatedProgress),
			                                                                   piece.angle * estimatedProgress);

			return TrackTransform.Lerp(firstLanePosition, lastLanePosition, estimatedProgress);
		}
    }
}

