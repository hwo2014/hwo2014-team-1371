//
// TrackTransform.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;

namespace Data
{
	[Serializable]
	public class TrackTransform
    {
		public Position position = new Position();
		public double angle;

		// Ensure angle is between -180 and 180;
		public static double FixAngle(double angle)
		{
			return (((angle + 180) % 360) + 360) % 360 - 180; // C# % can return negative values;
		}

		public TrackTransform()
		{
			angle = 0;
		}

		public TrackTransform(double x, double y, double angle)
		{
			this.position.x = x;
			this.position.y = y;
			this.angle = FixAngle(angle);
		}

		public TrackTransform(Position position, double angle)
		{
			this.position = position;
			this.angle = FixAngle(angle);
		}

		public TrackTransform(double angle)
		{
			this.position = new Position();
			this.angle = FixAngle(angle);
		}

		public TrackTransform(Position position)
		{
			this.position = position;
			this.angle = 0;
		}

		public static TrackTransform operator +(TrackTransform a, TrackTransform b)
		{
			return new TrackTransform(a.position + b.position, FixAngle(a.angle + b.angle));
		}

		public static TrackTransform operator -(TrackTransform a, TrackTransform b)
		{
			return new TrackTransform(a.position - b.position, FixAngle(a.angle - b.angle));
		}
		
		public static TrackTransform operator *(TrackTransform a, TrackTransform b)
		{
				return new TrackTransform(a * b.position, FixAngle(a.angle + b.angle));
		}

		public static Position operator *(TrackTransform a, Position p)
		{
			return p.Rotated(a.angle) + a.position;
		}
		
		public static TrackTransform Lerp(TrackTransform a, TrackTransform b, double f)
		{
			double delta = FixAngle(b.angle - a.angle);
			double newAngle = FixAngle(a.angle + f * delta);
			return new TrackTransform(Position.Lerp(a.position, b.position, f), newAngle);
		}
    }
}

