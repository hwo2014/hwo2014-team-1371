//
// CarPosition.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;

namespace Data
{
	// Unity doesn't serialize List<array> it seems.
	[Serializable]
	public class CarPositions
	{
		public CarPosition[] positions;
	}

	[Serializable]
	public class CarPosition
	{
		[Serializable]
		public class PiecePosition
		{
			[Serializable]
			public class Lane
			{
				public int startLaneIndex;
				public int endLaneIndex;

				public Lane()
				{
				}

				public Lane(Lane other)
				{
					startLaneIndex = other.startLaneIndex;
					endLaneIndex = other.endLaneIndex; 
				}
			}
			
			public int pieceIndex;
			public double inPieceDistance;
			public int lap;
			public Lane lane = new Lane();

			public PiecePosition()
			{
			}
			
			public PiecePosition(PiecePosition other)
			{
				pieceIndex = other.pieceIndex;
				inPieceDistance = other.inPieceDistance;
				lap = other.lap;
				lane = new Lane(other.lane);
			}

			public PiecePosition(int pieceId, int startLaneIndex, int endLaneIndex)
			{
				pieceIndex = pieceId;
				inPieceDistance = 0;
				lap = 0;
				lane.startLaneIndex = startLaneIndex;
				lane.endLaneIndex = endLaneIndex;
			}
		}

		public CarId id;
		public double angle;
		public bool crashed = false;
		public PiecePosition piecePosition;

		public CarPosition()
		{
			piecePosition = new PiecePosition();
		}

		public CarPosition(CarPosition other)
		{
			id = other.id;
			angle = other.angle;
			crashed = other.crashed;
			piecePosition = new PiecePosition(other.piecePosition);
		}
	}
}
