//
// Position.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;

namespace Data
{
	[Serializable]
	public class Position
    {
        public double x;
		public double y;

		public Position()
		{
			x = 0;
			y = 0;
		}

		public Position(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public static Position operator -(Position a, Position b)
		{
			return new Position(a.x - b.x, a.y - b.y);
		}

		public static Position operator +(Position a, Position b)
		{
			return new Position(a.x + b.x, a.y + b.y);
		}

		public static Position operator *(Position a, Position b)
		{
			return new Position(a.x * b.x, a.y * b.y);
		}

		public static Position operator *(Position a, double f)
		{
			return new Position(a.x * f, a.y * f);
		}

		public static Position operator /(Position a, double f)
		{
			return new Position(a.x / f, a.y / f);
		}

		public static Position Lerp(Position a, Position b, double f)
		{
			return a * (1 - f) + b * f;
		}
		
		public Position Rotated(double angle)
		{
			double angleRad = angle / 360 * Math.PI * 2;
			double sin = Math.Sin(angleRad);
			double cos = Math.Cos(angleRad);
			return new Position(x * cos + y * sin, y * cos - x * sin);
		}

		public double magnitude { get { return Math.Sqrt(x * x + y * y);} }
    }
}

