//
// TrackGraph.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using System.Collections.Generic;

namespace Data
{
	[Serializable]
    public class TrackGraph
    {
		[Serializable]
		public class Node
		{
			public int id;
			public List<int> edgesIds = new List<int>();
			public double minDistanceToLapEnd;
			public double maxDistanceToLapEnd;
			public int pieceId;
			public int laneId;
		}

		[Serializable]
		public class Edge
		{
			public int id;
			public double length;
			public double distanceToLapEnd;
			public int startNodeId;
			public int endNodeId;
			public int laneOffset;

			public List<int> samples = new List<int>();
		}

		[Serializable]
		public class Sample
		{
			public double maxLinearVelocity;
			public double linearAcceleration;
			public double maxSlipAngle;
			public double angularVelocityAtMaxSlipAngle;
		}

		public double sampleDistance = 10;

		// Only indices are kept in each class, otherwise it tends to die when Unity hotReloads :/
		public List<Node> nodes = new List<Node>();
		public List<Edge> edges = new List<Edge>();
		public List<Sample> samples = new List<Sample>();

		public double minLapLength;

		public Edge GetCurrentEdge(Track track, CarPosition.PiecePosition position)
		{
			int pieceId = position.pieceIndex;
			int laneId = position.lane.startLaneIndex;
			var node = nodes[pieceId * track.lanes.Length + laneId];
			int edgeOffset = (position.lane.endLaneIndex - laneId);
			int edgeId = (-edgeOffset + node.edgesIds.Count) % node.edgesIds.Count; // 0 = same, 1 = left, 2 = right;
			return edges[node.edgesIds[edgeId]];
		}

		public Sample GetClosestSample(Track track, CarPosition.PiecePosition position)
		{
			var edge = GetCurrentEdge(track, position);
			double progress = position.inPieceDistance / edge.length;
			int sampleId = (int) Math.Min(Math.Floor(progress * edge.samples.Count), edge.samples.Count - 1);
			return samples[edge.samples[sampleId]];
		}

		public void ClearSamples()
		{
			samples.Clear();
			foreach (var edge in edges)
			{
				for (int i = 0; i < edge.length / sampleDistance; ++i)
				{
					samples.Add(new Sample());
					edge.samples.Add(samples.Count - 1);
				}
			}
		}

        public void Build(Track track)
		{
			nodes.Clear();
			edges.Clear();

			int pieceCount = track.pieces.Length;
			int laneCount = track.lanes.Length;

			// Create one node per piece per lane
			for (int pieceId = 0; pieceId < pieceCount; ++pieceId)
			{
				for (int laneId = 0; laneId < laneCount; ++laneId)
				{
					var node = new Node();
					node.id = nodes.Count;
					node.pieceId = pieceId;
					node.laneId = laneId;
					nodes.Add(node);
				}
			}

			// Create edges
			for (int pieceId = 0; pieceId < pieceCount; ++pieceId)
			{
				for (int laneId = 0; laneId < laneCount; ++laneId)
				{
					var node = nodes[pieceId * laneCount + laneId];
					var nextNode = nodes[(pieceId + 1) % pieceCount * laneCount + laneId];
					var piece = track.pieces[pieceId];
					var edge = new Edge();
					edge.id = edges.Count;
					edge.startNodeId = node.id;
					edge.endNodeId = nextNode.id;
					edge.length = track.GetPieceLengthAtPosition(new CarPosition.PiecePosition(node.pieceId, laneId, laneId));
					edge.laneOffset = 0;
					for (int i = 0; i < edge.length / sampleDistance; ++i)
					{
						samples.Add(new Sample());
						edge.samples.Add(samples.Count - 1);
					}
					edges.Add(edge);
					node.edgesIds.Add(edge.id);

					// Create neighbors
					if (!piece.@switch)
						continue;

					if (laneId > 0)
					{
						// Left connection
						nextNode = nodes[(pieceId + 1) % pieceCount * laneCount + (laneId - 1)];
						edge = new Edge();
						edge.id = edges.Count;
						edge.startNodeId = node.id;
						edge.endNodeId = nextNode.id;
						edge.laneOffset = -1;
						edge.length = track.GetPieceLengthAtPosition(new CarPosition.PiecePosition(node.pieceId, laneId, laneId - 1));
						for (int i = 0; i < edge.length / sampleDistance; ++i)
						{
							samples.Add(new Sample());
							edge.samples.Add(samples.Count - 1);
						}
						edges.Add(edge);
						node.edgesIds.Add(edge.id);
					}

					if (laneId < laneCount - 1)
					{
						// Left connection
						nextNode = nodes[(pieceId + 1) % pieceCount * laneCount + (laneId + 1)];
						edge = new Edge();
						edge.id = edges.Count;
						edge.startNodeId = node.id;
						edge.endNodeId = nextNode.id;
						edge.laneOffset = 1;
						edge.length = track.GetPieceLengthAtPosition(new CarPosition.PiecePosition(node.pieceId, laneId, laneId + 1));
						for (int i = 0; i < edge.length / sampleDistance; ++i)
						{
							samples.Add(new Sample());
							edge.samples.Add(samples.Count - 1);
						}
						edges.Add(edge);
						node.edgesIds.Add(edge.id);
					}
				}
			}

			// Compute shortest path
			for (int pieceId = pieceCount - 1; pieceId >= 0; --pieceId)
			{
				for (int laneId = 0; laneId < laneCount; ++laneId)
				{
					var node = nodes[pieceId * laneCount + laneId];
					node.minDistanceToLapEnd = double.MaxValue;
					node.maxDistanceToLapEnd = double.MinValue;

					foreach (var edgeId in node.edgesIds)
					{
						var edge = edges[edgeId];
						edge.distanceToLapEnd = double.MaxValue;

						foreach (var nextEdgeId in nodes[edge.endNodeId].edgesIds)
						{
							var nextEdge = edges[nextEdgeId];
							edge.distanceToLapEnd = Math.Min(edge.distanceToLapEnd, edge.length + nextEdge.distanceToLapEnd);
						}

						node.minDistanceToLapEnd = Math.Min(node.minDistanceToLapEnd, edge.distanceToLapEnd);
						node.maxDistanceToLapEnd = Math.Max(node.maxDistanceToLapEnd, edge.distanceToLapEnd);
					}
				}
			}

			// Get min lap length
			minLapLength = double.MaxValue;
			for (int laneId = 0; laneId < laneCount; ++laneId)
			{
				var node = nodes[laneId];
				
				foreach (var edgeId in node.edgesIds)
				{
					var edge = edges[edgeId];
					minLapLength = Math.Min(minLapLength, edge.distanceToLapEnd);
				}
			}
		}
    }
}

