//
// PreferenceBehavior.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using UnityEngine;
using System.Collections.Generic;

public class PreferenceBehavior : MonoBehaviour
{
	public static PreferenceBehavior Instance
	{
		get
		{
			if (_instance == null)
			{
				var prefObj = GameObject.Find("Preferences");
				if (prefObj == null)
				{
					prefObj = new GameObject("Preferences");
					prefObj.AddComponent<PreferenceBehavior>();
				}
				_instance = prefObj.GetComponent<PreferenceBehavior>();
			}
			return _instance;
		}
	}

	private static PreferenceBehavior _instance;

	public int DisplayRange = 30;

	public List<CarBehavior.CarProperty> CarProperties = new List<CarBehavior.CarProperty>()
	{
		new CarBehavior.CarProperty(){Name = "linearVelocity", Scale = 10, ShowGizmo = true, Color = Color.red, Value = (car, ai) => car.linearVelocity},
		new CarBehavior.CarProperty(){Name = "linearAcceleration", Scale = 500, ShowGizmo = true, Color = Color.green, Value = (car, ai) => car.linearAcceleration},
		new CarBehavior.CarProperty(){Name = "angularVelocity", Scale = 30, ShowGizmo = false, Color = Color.red, Value = (car, ai) => car.angularVelocity},
		new CarBehavior.CarProperty(){Name = "angularAcceleration", Scale = 300, ShowGizmo = false, Color = Color.red, Value = (car, ai) => car.angularAcceleration},
		new CarBehavior.CarProperty(){Name = "trackAngle", Scale = 2, ShowGizmo = false, Color = Color.red, Value = (car, ai) => car.trackAngle},
		new CarBehavior.CarProperty(){Name = "trackTorque", Scale = 200, ShowGizmo = false, Color = Color.magenta, Value = (car, ai) => car.trackTorque},
		new CarBehavior.CarProperty(){Name = "slipAngle", Scale = 3, ShowGizmo = true, Color = Color.cyan, Value = (car, ai) => car.slipAngle},
		new CarBehavior.CarProperty(){Name = "raceProgress", Scale = 1, ShowGizmo = false, Color = Color.white, Value = (car, ai) => car.raceProgress},
		new CarBehavior.CarProperty(){Name = "desiredThrottle", Scale = 100, ShowGizmo = true, Color = Color.yellow, Value = (car, ai) => ai != null ? ai.desiredThrottle : 0},
		new CarBehavior.CarProperty(){Name = "desiredVelocity", Scale = 100, ShowGizmo = true, Color = Color.magenta, Value = (car, ai) => ai != null ? ai.desiredVelocity : 0},
		new CarBehavior.CarProperty(){Name = "expectedAcceleration", Scale = 500, ShowGizmo = false, Color = Color.blue, Value = (car, ai) => ai != null ? ai.expectedAcceleration : 0},
		new CarBehavior.CarProperty(){Name = "lookAheadScore", Scale = 200, ShowGizmo = false, Color = Color.blue, Value = (car, ai) => ai != null ? ai.lookAheadScore : 0},
		new CarBehavior.CarProperty(){Name = "crashTimeMargin", Scale = 200, ShowGizmo = false, Color = Color.blue, Value = (car, ai) => ai != null ? ai.crashTimeMargin : 0},
		new CarBehavior.CarProperty(){Name = "emergencyBrake", Scale = 200, ShowGizmo = false, Color = Color.blue, Value = (car, ai) => ai != null ? ai.emergencyBrake : 0},
		new CarBehavior.CarProperty(){Name = "estimatedMaxVelocity", Scale = 200, ShowGizmo = false, Color = Color.blue, Value = (car, ai) => ai != null ? ai.estimatedTrackMaxVelocity : 0},
	};
}

