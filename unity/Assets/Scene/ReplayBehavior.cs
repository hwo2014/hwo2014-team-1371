//
// ReplayBehavior.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using UnityEngine;
using AI;
using System;
using Data;
using System.Collections.Generic;

public class ReplayBehavior : MonoBehaviour
{
	public Bot Bot;
	public Transform Track;
	public Transform Cars;
	public CarBehavior ActiveCar;
	public Dictionary<string, Transform> CarMap = new Dictionary<string, Transform>();
	public List<Transform> Pieces = new List<Transform>();
	[NonSerialized] public string GameId;
	public int DisplayedTick;
	public static float CarHeight = 5.0f;
	public static Dictionary<string, Color> ColorMap = new Dictionary<string, Color>
	{
		{"red", Color.red},
		{"blue", Color.blue},
		{"black", Color.black},
		{"cyan", Color.cyan},
		{"gray", Color.gray},
		{"green", Color.green},
		{"grey", Color.grey},
		{"magenta", Color.magenta},
		{"white", Color.white},
		{"yellow", Color.yellow},
		{"orange", Color.magenta},
		{"purple", Color.magenta},
		{"lightred", Color.magenta},
	};

	public void Clear()
	{
		while (transform.childCount > 0)
			DestroyImmediate(transform.GetChild(0).gameObject);

		CarMap.Clear();
		Pieces.Clear();
		GameId = null;
		DisplayedTick = 0;
	}

	public void Rebuild(bool force)
	{
		var raceStatus = Bot.RaceStatus;
		if (raceStatus == null)
			return;

		if (raceStatus.GameId != null && raceStatus.GameId == GameId && !force)
			return;

		Clear();

		if (!raceStatus.Initialized)
			return;

		GameId = raceStatus.GameId;

		Track = new GameObject("Track").transform;
		Track.parent = transform;
		Track.transform.localPosition = Vector3.zero;
		Track.transform.localRotation = Quaternion.identity;
		//Track.hideFlags = HideFlags.DontSave;

		BuildTrack(raceStatus);

		Cars = new GameObject("Cars").transform;
		Cars.parent = transform;
		Cars.transform.localPosition = Vector3.zero;
		Cars.transform.localRotation = Quaternion.identity;
		//Cars.hideFlags = HideFlags.DontSave;

		BuildCars(raceStatus);

		SetTick(0);
	}

	public void SetTick(int tick)
	{
		var raceStatus = Bot.RaceStatus;
		if (raceStatus == null)
			return;

		if (!raceStatus.Initialized)
			return;

		tick = Mathf.Clamp(tick, 0, Mathf.Max(raceStatus.CarPositions.Count - 1, 0));
		DisplayedTick = tick;

		if (tick >= raceStatus.CarPositions.Count)
			return;

		CarPositions carPositions = raceStatus.CarPositions[tick];
		var track = raceStatus.GameInit.race.track;

		foreach (var position in carPositions.positions)
		{
			Transform car = null;
			if (!CarMap.TryGetValue(position.id.color, out car))
				continue;

			TrackTransform trackTransform = track.GetTrackTransform(position);
			car.localPosition = new Vector3((float)trackTransform.position.x, (float)trackTransform.position.y, 0);
			car.localRotation = Quaternion.Euler(0, 0, (float)(-trackTransform.angle - position.angle));
		}
	}

	public static Vector3 GetLocalPosition(TrackTransform trackTransform)
	{
		return new Vector3((float)trackTransform.position.x, (float)trackTransform.position.y, 0);
	}

	public static Quaternion GetLocalRotation(TrackTransform trackTransform)
	{
		return Quaternion.Euler(0, 0, (float)(-trackTransform.angle));
	}

	void BuildCars(RaceStatus raceStatus)
	{
		var cars = raceStatus.GameInit.race.cars;
		CarMap.Clear();
		int i = 0;
		foreach (var car in cars)
		{
			var carObject = new GameObject(String.Format("{0} ({1})", car.id.name, car.id.color));
			carObject.transform.parent = Cars;
			if (Pieces.Count > 0)
			{
				carObject.transform.localPosition = Pieces[0].transform.localPosition;
				carObject.transform.localRotation = Pieces[0].transform.localRotation;
			}
			CarBehavior carBehavior = carObject.AddComponent<CarBehavior>();
			carBehavior.CarId = car.id;
			carBehavior.CarListIndex = i;
			carBehavior.RaceStatus = raceStatus;
			carBehavior.RaceAnalysis = Bot.RaceAnalysis;
			carBehavior.RaceAI = i == raceStatus.OwnCarListIndex ? Bot.RaceAI : null;
			carBehavior.Replay = this;

			MeshFilter meshFilter = carObject.AddComponent<MeshFilter>();
			MeshRenderer meshRenderer = carObject.AddComponent<MeshRenderer>();
			meshRenderer.material = new Material(Shader.Find("Diffuse"));
			Color color;
			if (!ColorMap.TryGetValue(car.id.color, out color))
				color = Color.magenta;
			meshRenderer.sharedMaterial.color = color;
			Mesh mesh = BuildCar(car, CarHeight);
			meshFilter.mesh = mesh;
			CarMap.Add(car.id.color, carObject.transform);
			//carObject.hideFlags = HideFlags.DontSave;
			++i;
		}
	}

	Mesh BuildCar(GameInit.Car car, float height)
	{
		float width = (float)car.dimensions.width;
		float length = (float)car.dimensions.length;
		float guideOffset = (float)car.dimensions.guideFlagPosition - length;

		Vector3[] vertices = new Vector3[]
		{
			new Vector3(-width * 0.5f, guideOffset, 0),
			new Vector3(width * 0.5f, guideOffset, 0),
			new Vector3(-width * 0.5f, guideOffset + length, 0),
			new Vector3(width * 0.5f, guideOffset + length, 0),
			new Vector3(-width * 0.5f, guideOffset, -height),
			new Vector3(width * 0.5f, guideOffset, -height),
			new Vector3(-width * 0.5f, guideOffset + length, -height),
			new Vector3(width * 0.5f, guideOffset + length, -height),
		};
		
		Vector3[] normals = new Vector3[]
		{
			Vector3.forward,
			Vector3.forward,
			Vector3.forward,
			Vector3.forward,
			Vector3.back,
			Vector3.back,
			Vector3.back,
			Vector3.back,
		};
		
		Vector2[] uvs = new Vector2[]
		{
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
		};
		
		int[] triangles = new int[]
		{
		// Bottom
			0, 1, 2,
			2, 1, 3,

		// Top
			4, 6, 5,
			5, 6, 7,

		// Back
			0, 4, 1,
			1, 4, 5,

		// Right
			2, 6, 0,
			0, 6, 4,

		// Left
			1, 5, 3,
			3, 5, 7,

		// Front
			3, 7, 2,
			2, 7, 6,
		};
		
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.normals = normals;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		
		mesh.RecalculateBounds();
		mesh.Optimize();
		
		return mesh;
	}

	void BuildTrack(RaceStatus raceStatus)
	{
		Pieces.Clear();
		var track = raceStatus.GameInit.race.track;
		track.Build();
		int pieceId = 0;
		float width = 0;
		foreach (var lane in track.lanes)
		{
			width = Mathf.Max(width, Mathf.Abs((float)lane.distanceFromCenter) + 20);
		}
		width *= 2;

		foreach (var piece in track.pieces)
		{
			var pieceObject = new GameObject(String.Format("Piece_{0}", pieceId));
			pieceObject.transform.parent = Track;
			pieceObject.transform.localPosition = new Vector3((float)piece.startPoint.position.x, (float)piece.startPoint.position.y, 0);
			pieceObject.transform.localRotation = Quaternion.Euler(0, 0, (float)-piece.startPoint.angle);
			MeshFilter meshFilter = pieceObject.AddComponent<MeshFilter>();
			MeshRenderer meshRenderer = pieceObject.AddComponent<MeshRenderer>();
			var material = (Material)Resources.Load(piece.@switch ? "Track_Switch" : "Track_Empty", typeof(Material));
			meshRenderer.sharedMaterial = material;
			Mesh mesh = piece.length <= 0.01
				? BuildCurvePiece(piece, width)
				: BuildStraightPiece(piece, width);
			meshFilter.mesh = mesh;
			//pieceObject.hideFlags = HideFlags.DontSave;
			++pieceId;
			Pieces.Add(pieceObject.transform);
		}

		var trackBehavior = Track.gameObject.AddComponent<TrackBehavior>();
		trackBehavior.Replay = this;
		trackBehavior.RaceStatus = raceStatus;
		trackBehavior.RaceAnalysis = Bot.RaceAnalysis;
	}

	Mesh BuildStraightPiece(Track.Piece piece, float width)
	{
		Vector3[] vertices = new Vector3[]
		{
			new Vector3(-width * 0.5f, 0, 0),
			new Vector3(width * 0.5f, 0, 0),
			new Vector3(-width * 0.5f, (float)piece.length, 0),
			new Vector3(width * 0.5f, (float)piece.length, 0),
		};

		Vector3[] normals = new Vector3[]
		{
			Vector3.back,
			Vector3.back,
			Vector3.back,
			Vector3.back,
		};
		
		Vector2[] uvs = new Vector2[]
		{
			new Vector2(0, 0),
			new Vector2(1, 0),
			new Vector2(0, 1),
			new Vector2(1, 1),
		};

		int[] triangles = new int[]
		{
			0, 2, 1,
			1, 2, 3
		};

		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.normals = normals;
		mesh.triangles = triangles;
		mesh.uv = uvs;

		mesh.RecalculateBounds();
		mesh.Optimize();

		return mesh;
	}

	Mesh BuildCurvePiece(Track.Piece piece, float width)
	{
		int segments = 4;
		int vertexCount = (segments + 1) * 2;
		Vector3[] vertices = new Vector3[vertexCount];
		Vector3[] normals = new Vector3[vertexCount];	
		Vector2[] uvs = new Vector2[vertexCount];
		int[] triangles = new int[segments * 2 * 3];
		float pieceAngle = (float)piece.angle;
		float pieceRadius = (float)piece.radius;

		for (int i = 0; i <= segments; ++i)
		{
			float progress = (float)i / segments;
			float angle = Mathf.Lerp(0, pieceAngle, progress);
			Vector3 center = new Vector3(Mathf.Sign(angle) * pieceRadius, 0, 0);
			Quaternion rotation = Quaternion.Euler(0, 0, -angle);
			Vector3 position = center - rotation * new Vector3(Mathf.Sign(angle) * pieceRadius, 0, 0);

			vertices[2 * i] = position - rotation * new Vector3(width * 0.5f, 0, 0);
			vertices[2 * i + 1] = position + rotation * new Vector3(width * 0.5f, 0, 0);
			normals[2 * i] = Vector3.back;
			normals[2 * i + 1] = Vector3.back;
			uvs[2 * i] = new Vector2(0, progress);
			uvs[2 * i + 1] = new Vector2(1, progress);
		}

		for (int i = 0; i < segments; ++i)
		{
			triangles[6 * i] = 2 * i;
			triangles[6 * i + 1] = 2 * i + 2;
			triangles[6 * i + 2] = 2 * i + 1;
			triangles[6 * i + 3] = 2 * i + 1;
			triangles[6 * i + 4] = 2 * i + 2;
			triangles[6 * i + 5] = 2 * i + 3;
		}
			
		Mesh mesh = new Mesh();
		mesh.vertices = vertices;
		mesh.normals = normals;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		
		mesh.RecalculateBounds();
		mesh.Optimize();

		return mesh;
	}
}