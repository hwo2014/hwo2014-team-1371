//
// CarBehavior.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using System.Collections.Generic;
using UnityEngine;
using AI;
using Data;

public class CarBehavior : MonoBehaviour
{
	[Serializable]
	public class CarProperty
	{
		public string Name;
		public double Scale;
		public bool ShowGizmo;
		public Color Color;
		public ReadProperty Value = ((car, ai) => double.NaN);

		public delegate double ReadProperty(CarTickData carTickData, AITickData aiTickData);
	}

	public int CarListIndex;
	public CarId CarId;
	public RaceStatus RaceStatus;
	public RaceAnalysis RaceAnalysis;
	public RaceAI RaceAI;
	public ReplayBehavior Replay;

	// Used by animation curves
	public float ReplayTick;
	public float LastReplayTick;

	public void OnDrawGizmosSelected()
	{
		if (RaceStatus == null || RaceAnalysis == null || Replay == null || CarId == null)
			return;

		int start = Math.Max(Replay.DisplayedTick - PreferenceBehavior.Instance.DisplayRange, 0);
		int end = Math.Min(Replay.DisplayedTick, RaceAnalysis.CarTickData.Count);
		for (int i = start; i < end; ++i)
		{
			var carTickData = RaceAnalysis.CarTickData[i].tickData[CarListIndex];
			var aiTickData = RaceAI != null && i < RaceAI.AITickData.Count ? RaceAI.AITickData[i] : null;
			var position = ReplayBehavior.GetLocalPosition(carTickData.position) + Vector3.back * 10;
			var rotation = ReplayBehavior.GetLocalRotation(carTickData.position);
			var carProperties = PreferenceBehavior.Instance.CarProperties;
			for (int p = 0; p < carProperties.Count; ++p)
			{
				CarProperty property = carProperties[p];
				if (!property.ShowGizmo)
					continue;
				Gizmos.color = property.Color;
				Vector3 offset = rotation * Vector3.left * (float) (property.Scale * property.Value(carTickData, aiTickData));
				Vector3 shift = rotation * Vector3.up * (float) carTickData.linearVelocity * (float) p / carProperties.Count;
				Gizmos.DrawLine(position + shift, position + shift + offset);
			}

			if (aiTickData != null)
			{
				Vector3 lookAheadPosition = ReplayBehavior.GetLocalPosition(aiTickData.lookAheadPosition);
				Gizmos.color = new HSBColor(Mathf.Lerp(0.0f, 0.35f, (float) aiTickData.lookAheadScore), 1.0f, 1.0f).ToColor();
				Gizmos.DrawWireSphere(lookAheadPosition, 2.5f);
			}
		}
	}
}

