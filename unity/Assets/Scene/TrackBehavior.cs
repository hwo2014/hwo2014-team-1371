//
// TrackBehavior.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using UnityEngine;
using AI;
using Data;

public class TrackBehavior : MonoBehaviour
{
	public RaceStatus RaceStatus;
	public RaceAnalysis RaceAnalysis;
	public RaceAI RaceAI;
	public ReplayBehavior Replay;

	public void OnDrawGizmos()
	{
		if (RaceStatus == null || RaceAnalysis == null || Replay == null)
			return;
		
		if (!RaceStatus.Initialized)
			return;

		if (RaceAI == null && Replay != null)
			RaceAI = Replay.Bot.RaceAI;

		var track = RaceStatus.GameInit.race.track;
		var trackGraph = track.trackGraph;

		var topSpeed = RaceAI == null ? 10 : RaceAI.TopSpeed;

		// Display track graph
		foreach (var node in track.trackGraph.nodes)
		{
			Gizmos.color = node.edgesIds.Count > 1 ? Color.cyan : Color.black;
			var firstEdge = trackGraph.edges[node.edgesIds[0]];
			var piecePosition = new CarPosition.PiecePosition(node.pieceId, node.laneId, node.laneId + firstEdge.laneOffset);
			var transform = track.GetTrackTransform(piecePosition);
			Vector3 position = ReplayBehavior.GetLocalPosition(transform);
			Quaternion rotation = ReplayBehavior.GetLocalRotation(transform);
			Gizmos.DrawWireSphere(position, 2f);

			foreach (var edgeId in node.edgesIds)
			{
				var edge = trackGraph.edges[edgeId];
				double distanceAmplitude = (node.maxDistanceToLapEnd - node.minDistanceToLapEnd);
				if (node.edgesIds.Count > 1)
				{
					if (edge.laneOffset != 0 && (edge.distanceToLapEnd - node.minDistanceToLapEnd) / distanceAmplitude < 0.1f)
					{
						Gizmos.color = Color.cyan;
						Gizmos.DrawLine(position, position + rotation * new Vector3(edge.laneOffset, 1, 0) * 10);
					}
				}
				Vector3 start = position;
				int segments = (int) (edge.length / trackGraph.sampleDistance);
				for (int i = 0; i < segments; ++i)
				{
					var sample = i < edge.samples.Count ? trackGraph.samples[edge.samples[i]] : null;
					double sampleVelocityRatio = sample != null ? sample.maxLinearVelocity / topSpeed : 0;
					//double timeMarginRatio = sample != null && RaceAI != null ? RaceAI.GetTimeMargin(sample.maxSlipAngle, sample.angularVelocityAtMaxSlipAngle) / RaceAI.MaxTimeMargin : 0;
					double timeMarginRatio = sample != null && RaceAI != null ? sample.maxSlipAngle / RaceAI.MaxSlipAngle : 0;
					var color = new HSBColor(Mathf.Lerp(0f, 0.35f, (float) timeMarginRatio), 1.0f, Mathf.Lerp(0, 1, (float) sampleVelocityRatio)).ToColor();
					double endProgress = (double) (i + 1) / segments;
					var lookPosition = track.LookAhead(new CarPosition.PiecePosition(node.pieceId, node.laneId, node.laneId + edge.laneOffset), endProgress * edge.length, Track.LaneSwitch.None);
					Vector3 end = ReplayBehavior.GetLocalPosition(track.GetTrackTransform(lookPosition));
					Gizmos.color = color;
					Gizmos.DrawLine(start, end);
					start = end;
				}
			}
		}
	}
}

