//
// Msg.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using Newtonsoft.Json;
using System;

namespace Messages
{
	public class Msg
	{
		public string msgType;

		[NonSerialized]	private string _json = null;
		[NonSerialized]	private string _jsonIndented = null;
		private EMsgDirection _msgDirection = EMsgDirection.Unknown;

		public string ToJson(bool indented, bool forceRefresh)
		{
			if (forceRefresh)
			{
				_jsonIndented = null;
				_json = null;
			}

			if (indented)
			{
				if (_jsonIndented == null)
					_jsonIndented = JsonConvert.SerializeObject(this, Formatting.Indented);
				return _jsonIndented;
			}
			else
			{
				if (_json == null)
					_json = JsonConvert.SerializeObject(this, Formatting.None);
				return _json;
			}
		}

		public enum EMsgDirection
		{
			Sent,
			Received,
			Unknown
		}

		public EMsgDirection MsgDirection()
		{
			return _msgDirection;
		}

		public void SetMsgDirection(EMsgDirection msgDirection)
		{
			_msgDirection = msgDirection;
		}
	}

	[Serializable]
	public class Msg<T> : Msg
	{
		public T data;
	}
}

