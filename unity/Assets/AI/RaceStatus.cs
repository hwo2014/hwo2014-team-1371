//
// RaceStatus.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Data;
using System.Collections.Generic;

namespace AI
{
	[Serializable]
    public class RaceStatus
    {
		public bool Initialized = false;
		public string GameId = null;
		public int LastGameTick;
		public int OwnCarListIndex;
		public CarId OwnCarId = null;
		public CarId[] CarIds = null;
		public CarPosition[] LastCarPositions = null;
		public CarPosition LastOwnCarPosition = null;
		public List<CarPositions> CarPositions = new List<CarPositions>();
		public List<LapFinished> LapsFinished = new List<LapFinished>();
		public LapFinished LastOwnLap = null;
		public GameInit GameInit = null;
		public GameEnd GameEnd = null;
		public GameEnd.Result OwnResult = null;
		public GameEnd.BestLap OwnBestLap = null;

		// Turbo
		public TurboAvailable LastTurboAvailable = null;
		public bool LastTurboActive = false;
    }
}

