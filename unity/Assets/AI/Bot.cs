//
// Bot.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System.Collections.Generic;
using Messages;
using Data;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Diagnostics;

namespace AI
{
	[Serializable]
	public class Bot
	{
		public string BotName = "Fre's bot";
		public string BotKey = "pJNXHbyiYlXnDw";
		public string ServerName = "hakkinen.helloworldopen.com";
		public int ServerPort = 8091;
		public int AITimeout = 50;
		public int AICheckInterval = 1;
		public int AISleepDuration = 1;
		public int TicksBehind = 0;
		public List<string> Log = new List<string>();

		public double AverageTimePerAIMessageSent = 0;
		public double AverageTimePerAIUpdate = 0;
		public double UpdateAverageStrength = 0.1;

		[NonSerialized]	public List<Msg> Messages = new List<Msg>();
		[NonSerialized]	public Msg NextThrottleMsg = null;
		[NonSerialized]	public Msg NextLaneSwitchMsg = null;
		[NonSerialized]	public Msg NextTurboMsg = null;

		public int ReceivedCount { get; private set; }
		public int SentCount { get; private set; }

		public RaceStatus RaceStatus = new RaceStatus();
		public RaceAnalysis RaceAnalysis = new RaceAnalysis();
		public RaceAI RaceAI = new RaceAI();

		public bool Ready { get { return _ready; } }
		public bool Running { get { return _running; } }
		public bool Completed { get { return _completed; } }
		public bool RequestingAbort { get { return _abort; } }

		[NonSerialized] private StreamWriter _writer;
		[NonSerialized]	private StreamReader _reader;
		[NonSerialized]	private TcpClient _client;
		[NonSerialized]	private Thread _readThread;
		[NonSerialized]	private Thread _aiThread;
		[NonSerialized]	private bool _ready = false;
		[NonSerialized]	private bool _abort = false;
		[NonSerialized]	private bool _running = false;
		[NonSerialized]	private bool _completed = false;
		[NonSerialized]	private Msg _joinMsg = null;
		[NonSerialized]	private bool _threaded = true;
		[NonSerialized]	private Stopwatch _AIUpdateStopwatch = new Stopwatch();
		[NonSerialized]	private Stopwatch _AIMessageStopwatch = new Stopwatch();

		public Bot Clone()
		{
			IFormatter formatter = new BinaryFormatter();
			Stream stream = new MemoryStream();
			using (stream)
			{
				formatter.Serialize(stream, this);
				stream.Seek(0, SeekOrigin.Begin);
				return (Bot)formatter.Deserialize(stream);
			}
		}

		public Bot(bool threaded)
		{
            _threaded = threaded;
		}

		public Bot(Bot other)
		{
			BotName = other.BotName;
			BotKey = other.BotKey;
			ServerName = other.ServerName;
			ServerPort = other.ServerPort;
			AICheckInterval = other.AICheckInterval;
			AITimeout = other.AITimeout;
			RaceAI = new RaceAI(other.RaceAI);

			_threaded = other._threaded;
		}

		public void QuickRace()
		{
			_joinMsg = new JoinMsg(BotName, BotKey);
			Start();
		}

		public void CreateRace(string trackName, string password, int carCount)
		{
			_joinMsg = new CreateRaceMsg(BotName, BotKey, trackName, password, carCount);
			Start();
		}

		public void JoinRace(string trackName, string password, int carCount)
		{
			_joinMsg = new JoinRaceMsg(BotName, BotKey, trackName, password, carCount);
			Start();
		}

		private void Start()
		{
			if (_running)
			{
				Stop();
			}

			Log.Clear();

			_running = true;
			ReceivedCount = 0;
			SentCount = 0;
			Messages.Clear();

			RaceStatus = new RaceStatus();
			RaceAnalysis = new RaceAnalysis();

			_AIUpdateStopwatch.Start();
			_AIMessageStopwatch.Start();

			_abort = false;
			if (_threaded)
			{
                Console.WriteLine("Bot started (Threaded)"); 
                _readThread = new Thread(SafeRead);
				_readThread.Start();
				_aiThread = new Thread(SafeUpdate);
				_aiThread.Start();
			}
			else
			{
                Console.WriteLine("Bot started (Synchronous)");
				Read();
			}
		}
		
		public void Stop()
		{
			if (!_running)
				return;
			_abort = true;
			_running = false;
			if (_client != null)
				_client.Close();
			if (_readThread != null)
				_readThread.Join();
			if (_aiThread != null)
				_aiThread.Join();
		}

		void SafeRead()
		{
			try
			{
				Read();
			}
			catch (Exception ex)
			{
				AddLog("Read exception: " + ex.Message + " (" + ex.Data.ToString() + ")");
				AddLog(ex.StackTrace);
				AddLog(ex.StackTrace);
				if (_client != null)
					_client.Close();
				_abort = true;
			}
		}

		void SafeUpdate()
		{
			try
			{
				Update();
			}
			catch (Exception ex)
			{
				AddLog("Update exception: " + ex.Message + " (" + ex.Data.ToString() + ")");
				AddLog(ex.StackTrace);
				if (_client != null)
					_client.Close();
				_abort = true;
			}
		}
		
		// Update is called once per frame
		void Read()
		{
			AddLog("Connecting to " + ServerName + ":" + ServerPort + " as " + BotName + "/" + BotKey);

			_client = new TcpClient(ServerName, ServerPort);
			NetworkStream stream = _client.GetStream();
			_reader = new StreamReader(stream);
			_writer = new StreamWriter(stream);
			_writer.AutoFlush = true;

			SendMsg(_joinMsg);

			string line;
			while ((line = _reader.ReadLine()) != null && !_abort)
			{
				Msg msg = JsonConvert.DeserializeObject<Msg<object>>(line);
				Msg<CarId> carMsg = null;
				Msg<LapFinished> lapMsg = null;
				Msg<GameInit> initMsg = null;
				Msg<GameEnd> endMsg = null;
				Msg<string> errorMsg = null;
				Msg<TurboAvailable> turboMsg = null;
				bool sendAIMsg = false;
				_ready = true;
				switch (msg.msgType)
				{
					case "carPositions":
						CarPositionsMsg carPositionsMsg = JsonConvert.DeserializeObject<CarPositionsMsg>(line);
						msg = carPositionsMsg;
						lock (RaceStatus)
						{
							RaceStatus.GameId = carPositionsMsg.gameId;
							var carPositions = new CarPositions(){positions = carPositionsMsg.data};
							RaceStatus.CarPositions.Add(carPositions);
							if (RaceStatus.LastCarPositions != null)
							{
								foreach (var carPosition in carPositions.positions)
								{
									foreach (var lastCarPosition in RaceStatus.LastCarPositions)
									{
										if (carPosition.id.SameCar(lastCarPosition.id))
											carPosition.crashed = lastCarPosition.crashed;
									}
								}
							}
							RaceStatus.LastCarPositions = carPositionsMsg.data;
							if (RaceStatus.OwnCarId != null)
							{
								foreach (var carPosition in RaceStatus.LastCarPositions)
								{
									if (carPosition.id.SameCar(RaceStatus.OwnCarId))
										RaceStatus.LastOwnCarPosition = carPosition;
								}
							}
							RaceStatus.LastGameTick = carPositionsMsg.gameTick;
						}
						sendAIMsg = true;
						break;
					case "join":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						AddLog(String.Format("Joined: {0} ({1})", carMsg.data.name, carMsg.data.color));
						break;
					case "yourCar":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						lock (RaceStatus)
						{
							RaceStatus.OwnCarId = carMsg.data;
						}
						AddLog(String.Format("Your car: {0} ({1})", carMsg.data.name, carMsg.data.color));
						break;
					case "gameInit":
						initMsg = JsonConvert.DeserializeObject<Msg<GameInit>>(line);
						AddLog(String.Format("Race initialized on: {0} ({1} cars - {2} laps)",
						                       initMsg.data.race.track.id,
						                       initMsg.data.race.cars.Length,
						                       initMsg.data.race.raceSession.laps));
						initMsg.data.race.track.Build();
						var carIds = new CarId[initMsg.data.race.cars.Length];
						int id = 0;
						int ownCarId = 0;
						foreach (GameInit.Car car in initMsg.data.race.cars)
						{
							carIds[id] = car.id;
							if (car.id.SameCar(RaceStatus.OwnCarId))
								ownCarId = id;
							++id;
						}
						lock (RaceStatus)
						{
							RaceStatus.OwnCarListIndex = ownCarId;
							RaceStatus.GameInit = initMsg.data;
							RaceStatus.CarIds = carIds;
							RaceStatus.Initialized = true;
						}
						break;
					case "gameEnd":
						endMsg = JsonConvert.DeserializeObject<Msg<GameEnd>>(line);
						string raceStatus = "Race ended\n";
						raceStatus += "---- Global rankings ----\n";
						int n = 0;
						lock (RaceStatus)
						{
							foreach (var result in endMsg.data.results)
							{
								++n;
								raceStatus += String.Format("#{0} {1:F2}s {2} ({3})\n", n,
								                        result.result.millis / 1000f,
								                        result.car.name,
								                        result.car.color);
								if (result.car.SameCar(RaceStatus.OwnCarId))
									RaceStatus.OwnResult = result;
							}
							raceStatus += "---- Best laps ----\n";
							n = 0;
							foreach (var result in endMsg.data.bestLaps)
							{
								++n;
								raceStatus += String.Format("#{0} {1:F2}s ({2}) {3} ({4})\n", n,
								                        result.result.millis / 1000f,
								                        result.result.lap + 1,
								                        result.car.name,
								                        result.car.color);
								if (result.car.SameCar(RaceStatus.OwnCarId))
									RaceStatus.OwnBestLap = result;
							}
							RaceStatus.GameEnd = endMsg.data;
						}
						AddLog(raceStatus);
						break;
					case "gameStart":
						AddLog("Race started");
						break;
					case "tournamentEnd":
						AddLog("Tournament ended");
						_client.Close();
						_completed = true;
						break;
					case "dnf":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						AddLog(String.Format("Disqualified: {0} ({1})", carMsg.data.name, carMsg.data.color));
						break;
					case "finish":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						AddLog(String.Format("Finished: {0} ({1})", carMsg.data.name, carMsg.data.color));
						break;
					case "crash":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						AddLog(String.Format("Crashed: {0} ({1})", carMsg.data.name, carMsg.data.color));
						lock (RaceStatus)
						{
							int carListIndex = 0;
							foreach (CarId car in RaceStatus.CarIds)
							{
								if (car.SameCar(carMsg.data))
								{
									var lastCarPosition = RaceStatus.LastCarPositions[carListIndex];
									lastCarPosition.crashed = true; // Maybe written on the wrong frame?
									break;
								}
								++carListIndex;
							}
						}
						break;
					case "spawn":
						carMsg = JsonConvert.DeserializeObject<Msg<CarId>>(line);
						AddLog(String.Format("Spawned: {0} ({1})", carMsg.data.name, carMsg.data.color));
						lock (RaceStatus)
						{
							int carListIndex = 0;
							foreach (CarId car in RaceStatus.CarIds)
							{
								if (car.SameCar(carMsg.data))
								{
									var lastCarPosition = RaceStatus.LastCarPositions[carListIndex];
									lastCarPosition.crashed = false; // Maybe written on the wrong frame?
									break;
								}
								++carListIndex;
							}
						}
						break;
					case "lapFinished":
						lapMsg = JsonConvert.DeserializeObject<Msg<LapFinished>>(line);
						AddLog(String.Format("Lap {0}: {1:F2}s ({2:F2}s) {3} ({4}) [#{5} overall, #{6} lap])",
						                       lapMsg.data.lapTime.lap + 1,
						                       lapMsg.data.raceTime.millis / 1000f,
						                       lapMsg.data.lapTime.millis / 1000f,
						                       lapMsg.data.car.name,
						                       lapMsg.data.car.color,
						                       lapMsg.data.ranking.overall,
						                       lapMsg.data.ranking.fastestLap));
						lock (RaceStatus)
						{
							if (RaceStatus.OwnCarId != null && lapMsg.data.car.SameCar(RaceStatus.OwnCarId))
							{
								RaceStatus.LastOwnLap = lapMsg.data;
							}
							RaceStatus.LapsFinished.Add(lapMsg.data);
						}
						break;
					case "error":
						errorMsg = JsonConvert.DeserializeObject<Msg<string>>(line);
						AddLog(String.Format("Error occured: {0}", errorMsg.data));
						_client.Close();
						break;
					case "turboAvailable":
						turboMsg = JsonConvert.DeserializeObject<Msg<TurboAvailable>>(line);
						AddLog(String.Format("Turbo available: x{0} for {1} ticks", turboMsg.data.turboFactor, turboMsg.data.turboDurationTicks));
						RaceStatus.LastTurboAvailable = turboMsg.data;
						break;
					case "turboStart":
						RaceStatus.LastTurboActive = true;
						AddLog(String.Format("Turbo started"));
						break;
					case "turboEnd":
						RaceStatus.LastTurboActive = false;
						RaceStatus.LastTurboAvailable = null;
						AddLog(String.Format("Turbo ended"));
						break;
					default:
					AddLog(String.Format("Unknown message type: {0} (ID: {1})", msg.msgType, Messages.Count.ToString()));
						break;
				}

				msg.SetMsgDirection(Msg.EMsgDirection.Received);
				lock (Messages)
				{
					Messages.Add(msg);
					ReceivedCount++;
				}

/*                if (!_threaded)
                    Console.WriteLine("<= " + msg.msgType);
*/
				if (!sendAIMsg || !SendNextAIMsg())
				{
					SendMsg(new PingMsg());
				}
			}
			_client.Close();
		}

		private void AddLog(string message)
		{
			Log.Add(message);
			if (!_threaded)
				Console.WriteLine(message);
		}
		
		private bool SendNextAIMsg()
		{
            Msg nextThrottleMsg = null;
			Msg nextLaneSwitchMsg = null;
			Msg nextTurboMsg = null;
			if (!_threaded)
            {
                UpdateOnce();
                nextThrottleMsg = NextThrottleMsg;
                NextThrottleMsg = null;
				nextTurboMsg = NextTurboMsg;
				NextTurboMsg = null;
				// Turbo has priority (lane switch has plenty of time to trigger
				if (nextTurboMsg == null)
				{
					nextLaneSwitchMsg = NextLaneSwitchMsg;
					NextLaneSwitchMsg = null;			
				}
            }
            else
            {
				int timeout = AITimeout;
                while (timeout > 0)
                {
                    Thread.Sleep(AICheckInterval);
					lock (RaceStatus)
					{
						if (RaceStatus.CarPositions.Count - RaceAI.AITickData.Count == 0)
						{
							nextThrottleMsg = NextThrottleMsg;
							NextThrottleMsg = null;
							nextTurboMsg = NextTurboMsg;
							NextTurboMsg = null;
							// Turbo has priority (lane switch has plenty of time to trigger
							if (nextTurboMsg == null)
							{
								nextLaneSwitchMsg = NextLaneSwitchMsg;
								NextLaneSwitchMsg = null;			
							}
							break;
						}
                    }
                    timeout -= AICheckInterval;
                }
			}

			if (nextTurboMsg != null)
				SendMsg(nextTurboMsg);
			else if (nextLaneSwitchMsg != null)
				SendMsg(nextLaneSwitchMsg);
            else if (nextThrottleMsg != null)
				SendMsg(nextThrottleMsg);
			else
				return false;

			_AIMessageStopwatch.Stop();
			AverageTimePerAIMessageSent = AverageTimePerAIMessageSent * (1 - UpdateAverageStrength) + (double) _AIMessageStopwatch.ElapsedMilliseconds * UpdateAverageStrength;
			_AIMessageStopwatch.Reset();
			_AIMessageStopwatch.Start();

			return true;
		}

		void Update()
		{
			while (!_abort)
			{
				if (!RaceStatus.Initialized)
				{
					Thread.Sleep(10);
					continue;
				}

				UpdateOnce();

				// Remove when AI becomes expensive
				Thread.Sleep(AISleepDuration);
			}
		}

		void UpdateOnce()
		{
			if (!RaceStatus.Initialized)
				return;

			lock (RaceStatus)
			{
				RaceAnalysis.ComputeRaceProgress(RaceStatus);
				RaceAnalysis.ComputeCarTickData(RaceStatus);
				RaceAI.ComputeDesiredThrottle(RaceStatus, RaceAnalysis);
			}
			
			lock (this)
			{
				if (RaceAI.NextDesiredTurbo)
				{
					NextLaneSwitchMsg = new TurboMsg();
					RaceAI.NextDesiredTurbo = false;
				}

				if (RaceAI.NextDesiredLaneSwitch != Track.LaneSwitch.None)
				{
					NextLaneSwitchMsg = new SwitchLaneMsg(RaceAI.NextDesiredLaneSwitch.ToString());
					RaceAI.NextDesiredLaneSwitch = Track.LaneSwitch.None;
				}
				
				NextThrottleMsg = new ThrottleMsg(RaceAI.NextDesiredThrottle);
			}

			_AIUpdateStopwatch.Stop();
			AverageTimePerAIUpdate = AverageTimePerAIUpdate * (1 - UpdateAverageStrength) + (double) _AIUpdateStopwatch.ElapsedMilliseconds * UpdateAverageStrength;
			_AIUpdateStopwatch.Reset();
			_AIUpdateStopwatch.Start();
		}
		
		void SendMsg(Msg msg)
		{
			msg.SetMsgDirection(Msg.EMsgDirection.Sent);
/*            if (!_threaded)
                Console.WriteLine("=> " + msg.msgType);
*/			_writer.WriteLine(msg.ToJson(false, true));
			lock (Messages)
			{
				SentCount++;
				Messages.Add(msg);
			}
		}
	}
}
