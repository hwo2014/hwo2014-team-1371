//
// RaceAnalysis.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using Data;

namespace AI
{
	[Serializable]
	public class RaceAnalysis
	{
		public List<CarTickDataArray> CarTickData = new List<CarTickDataArray>();
		public double RaceProgress;

		public int TickRate = 60; // Todo: use
		public int TrackGraphCommitDelay = 60;

		public void ComputeRaceProgress(RaceStatus raceStatus)
		{
			if (raceStatus.GameInit != null && raceStatus.LastOwnCarPosition != null)
			{
				RaceProgress = raceStatus.GameInit.race.GetRaceProgress(raceStatus.LastOwnCarPosition);
			}
			else
			{
				RaceProgress = 0;
			}
		}

		public void ComputeCarTickData(RaceStatus raceStatus)
		{
			if (!raceStatus.Initialized)
				return;

			var track = raceStatus.GameInit.race.track;

			var cars = raceStatus.CarIds;
			var carPositions = raceStatus.CarPositions;

			// Catch up on missing frames
			while (CarTickData.Count < carPositions.Count)
			{
				int nextTickId = CarTickData.Count;
				var positions = carPositions[nextTickId];
				var newTickDataArray = new CarTickDataArray();
				newTickDataArray.tickData = new CarTickData[cars.Length];
				var newTickData = newTickDataArray.tickData;
				var previousTickData = nextTickId > 0 ? CarTickData[nextTickId - 1].tickData : null;
				int oldTickId = nextTickId - TrackGraphCommitDelay;
				var oldTickData = oldTickId > 0 ? CarTickData[oldTickId - 1].tickData : null;

				for (int i = 0; i < cars.Length; ++i)
				{
					var carId = cars[i];
					var carTickData = new CarTickData();
					newTickData[i] = carTickData;
					var lastCarTickData = previousTickData == null ? null : previousTickData[i];
					var oldCarTickData = oldTickData == null ? null : oldTickData[i];
					// Find car position if available
					var carPosition = i < positions.positions.Length ? positions.positions[i] : null;
					if (carPosition == null || !carPosition.id.SameCar(carId))
					{
						carPosition = null;
						foreach (var position in positions.positions)
						{
							if (position.id.SameCar(carId))
							{
								carPosition = position;
								break;
							}
						}
					}
					if (carPosition == null)
					{
						carTickData.available = false;
						carTickData.crashed = false;

						// No car data available: use previous frame
						if (lastCarTickData == null)
						{
							carTickData.position = raceStatus.GameInit.race.track.startingPoint;
							carTickData.raceProgress = 0;
						}
						else
						{
							carTickData.position = lastCarTickData.position;
							carTickData.slipAngle = lastCarTickData.slipAngle;
							carTickData.linearVelocity = lastCarTickData.linearVelocity;
							carTickData.linearAcceleration = lastCarTickData.linearAcceleration;
							carTickData.angularVelocity = lastCarTickData.angularAcceleration;
							carTickData.trackAngle = lastCarTickData.trackAngle;
							carTickData.trackTorque = lastCarTickData.trackTorque;
						}
					}
					else
					{
						carTickData.available = true;
						carTickData.crashed = carPosition.crashed;
						carTickData.piecePosition = new CarPosition.PiecePosition(carPosition.piecePosition);
						carTickData.position = track.GetTrackTransform(carPosition);
						carTickData.raceProgress = raceStatus.GameInit.race.GetRaceProgress(carPosition);
						carTickData.slipAngle = carPosition.angle;

						if (lastCarTickData != null)
						{
							var transformDelta = carTickData.position - lastCarTickData.position;
							carTickData.linearVelocity = transformDelta.position.magnitude;
							carTickData.linearAcceleration = carTickData.linearVelocity - lastCarTickData.linearVelocity;
							carTickData.angularVelocity = carTickData.slipAngle - lastCarTickData.slipAngle;
							carTickData.angularAcceleration = carTickData.angularVelocity - lastCarTickData.angularVelocity;
							var piece = track.pieces[carPosition.piecePosition.pieceIndex];
							carTickData.trackAngle = piece.angle;
							carTickData.trackTorque = track.GetPieceTorque(carPosition);

							// Remove spikes at lap end (gives better data)
							if (carPosition.piecePosition.pieceIndex == 0 && carTickData.linearVelocity > carPosition.piecePosition.inPieceDistance)
							{
								carTickData.linearVelocity = lastCarTickData.linearVelocity;
								carTickData.linearAcceleration = lastCarTickData.linearAcceleration;
								carTickData.angularVelocity = lastCarTickData.angularVelocity;
								carTickData.angularAcceleration = lastCarTickData.angularAcceleration;
								carTickData.trackAngle = lastCarTickData.trackAngle;
								carTickData.trackTorque = lastCarTickData.trackTorque;
							}

							// Reset after crash
							if (lastCarTickData.linearVelocity > 1 && carTickData.linearVelocity < 0.01) // Cheap crash detection
							{
								carTickData.linearVelocity = 0;
								carTickData.linearAcceleration = 0;
								carTickData.angularVelocity = 0;
								carTickData.angularAcceleration = 0;
								carTickData.trackAngle = lastCarTickData.trackAngle;
								carTickData.trackTorque = lastCarTickData.trackTorque;
							}
						}
					}

					// Write to graph
					if (oldCarTickData != null && !carTickData.crashed)
					{
						var sample = track.GetClosestSample(oldCarTickData.piecePosition);
						if (oldCarTickData.linearVelocity > sample.maxLinearVelocity)
						{
							bool crashed = carTickData.crashed;
							double maxSlipAngle = carTickData.slipAngle;
							double angularVelocityAtMaxSlipAngle = carTickData.angularVelocity;
							
							// Record the max slip angle seen since then (= danger amount)
							for (int sampleTickId = nextTickId - 1; sampleTickId >= oldTickId; --sampleTickId)
							{
								var sampleTickData = CarTickData[nextTickId - 1].tickData[i];
								if (Math.Abs(sampleTickData.slipAngle) > Math.Abs(maxSlipAngle))
								{
									maxSlipAngle = sampleTickData.slipAngle;
									angularVelocityAtMaxSlipAngle = sampleTickData.angularVelocity;
								}
								crashed = crashed || sampleTickData.crashed;
							}

							if (!crashed)
							{
								sample.maxLinearVelocity = oldCarTickData.linearVelocity;
								sample.linearAcceleration = oldCarTickData.linearAcceleration;
								sample.maxSlipAngle = maxSlipAngle;
								sample.angularVelocityAtMaxSlipAngle = angularVelocityAtMaxSlipAngle;
							}
						}
					}
				}

				CarTickData.Add(newTickDataArray);
			}
		}
	}
}

