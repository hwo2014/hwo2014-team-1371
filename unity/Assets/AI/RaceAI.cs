//
// RaceAI.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using System.Collections.Generic;
using Data;

namespace AI
{
	[Serializable]
    public class RaceAI
    {
		public List<AITickData> AITickData = new List<AITickData>();

		public double NextDesiredThrottle = 1.0;
		public bool OverrideThrottle = false;
		public double NextDesiredVelocity = 1.0;
		public bool OverrideVelocity = false;
		public Track.LaneSwitch NextDesiredLaneSwitch = Track.LaneSwitch.None;
		public bool OverrideLaneSwitch = false;
		public bool NextDesiredTurbo = false;
		public bool OverrideTurbo = false;

		public double ThrottleAggressiveness = 100;
		public double LookAheadTime = 15;
		public double LookAheadDamping = 0.3;
		public double LookAheadResponse = 0.8;
		public double LookAheadTorqueWeight = 0.5;
		public double SlipAngleResponse = 0.5;
		public double TrackHistoryResponse = 0;
		public double MaxDesiredVelocity = 1;

		public double TopSpeed = 10;
		public double MaxSlipAngle = 60;
		public double AccelerationStrength = 0.02;

		public int MaxTimeMargin = 60;

		public double TurboFactor = 1;
		public int TurboDuration = 0;
		public bool TurboAvailable = false;
		public bool TurboActive = false;

		public TrackTransform LastLookAheadPosition = new TrackTransform();
		public double LastLookAheadScore = 1;

		public double LastTimeMargin = 0;
		public double LastEmergencyBrake = 0;
		public double LastTrackEstimatedMaxVelocity = 0;

		public RaceAI()
		{
		}

		public RaceAI(RaceAI other)
		{
			NextDesiredThrottle = other.NextDesiredThrottle;
			OverrideThrottle = other.OverrideThrottle;
			NextDesiredVelocity = other.NextDesiredVelocity;
			OverrideVelocity = other.OverrideVelocity;
			NextDesiredLaneSwitch = other.NextDesiredLaneSwitch;
			OverrideLaneSwitch = other.OverrideLaneSwitch;
			NextDesiredTurbo = other.NextDesiredTurbo;
			OverrideTurbo = other.OverrideTurbo;
			ThrottleAggressiveness = other.ThrottleAggressiveness;
			LookAheadTime = other.LookAheadTime;
			LookAheadDamping = other.LookAheadDamping;
			LookAheadTorqueWeight = other.LookAheadTorqueWeight;
			LookAheadResponse = other.LookAheadResponse;
			TrackHistoryResponse = other.TrackHistoryResponse;
			SlipAngleResponse = other.SlipAngleResponse;
			MaxDesiredVelocity = other.MaxDesiredVelocity;

			TopSpeed = other.TopSpeed;
			MaxSlipAngle = other.MaxSlipAngle;
			AccelerationStrength = other.AccelerationStrength;

			MaxTimeMargin = other.MaxTimeMargin;
		}

		public void ComputeDesiredThrottle(RaceStatus raceStatus, RaceAnalysis raceAnalysis)
		{
			if (!raceStatus.Initialized)
				return;

			if (raceAnalysis.CarTickData.Count == 0)
				return;

			if (raceStatus.LastTurboAvailable != null && !raceStatus.LastTurboActive)
			{
				TurboAvailable = true;
				TurboActive = false;
				TurboDuration = raceStatus.LastTurboAvailable.turboDurationTicks;
				TurboFactor = raceStatus.LastTurboAvailable.turboFactor;
			}
			else
			{
				TurboAvailable = false;
			}

			TurboActive = raceStatus.LastTurboActive;

			// Catch up on missing frames
			while (AITickData.Count < raceAnalysis.CarTickData.Count)
			{
				int frameId = AITickData.Count;
				var lastAiTickData = frameId > 0 ? AITickData[frameId - 1] : null;
				var lastCarTickData = frameId > 0 ? raceAnalysis.CarTickData[frameId - 1].tickData[raceStatus.OwnCarListIndex] : null;
				var carTickData = raceAnalysis.CarTickData[frameId].tickData[raceStatus.OwnCarListIndex];
				var carPosition = raceStatus.CarPositions[frameId].positions[raceStatus.OwnCarListIndex];
				var track = raceStatus.GameInit.race.track;
				var aiTickData = new AI.AITickData();
				var turboFactor = TurboActive ? TurboFactor : 1;

				// Switch lanes
				if (!OverrideLaneSwitch)
				{
					var edge = track.GetCurrentEdge(carPosition);
					var trackGraph = track.trackGraph;
					var endNode = trackGraph.nodes[edge.endNodeId];
					if (endNode.edgesIds.Count > 0)
					{
						// Will need to switch at the next node
						double minDistance = double.MaxValue;
						var bestEdge = trackGraph.edges[endNode.edgesIds[0]];
						int i = 0;
						foreach (var nextEdgeId in endNode.edgesIds)
						{
							var nextEdge = trackGraph.edges[nextEdgeId];
							if (nextEdge.distanceToLapEnd < minDistance)
							{
								bestEdge = trackGraph.edges[nextEdgeId];
								minDistance = nextEdge.distanceToLapEnd;
							}
							++i;
						}
						
						// This overrides throttle
						aiTickData.desiredLaneSwitch = Track.LaneSwitch.None;
						if (bestEdge.laneOffset > 0)
							aiTickData.desiredLaneSwitch = Track.LaneSwitch.Right;
						if (bestEdge.laneOffset < 0)
							aiTickData.desiredLaneSwitch = Track.LaneSwitch.Left;
					}

					if (lastAiTickData == null || aiTickData.desiredLaneSwitch != lastAiTickData.desiredLaneSwitch)
					{
						// Ensure message is sent only once.  WARNING: does not handle two switches one after the other
						NextDesiredLaneSwitch = aiTickData.desiredLaneSwitch;
					}
				}

				// Look ahead
				{
					var lookAheadPosition = track.LookAhead(carPosition.piecePosition, LookAheadTime * carTickData.linearVelocity, aiTickData.desiredLaneSwitch);
					var torque = track.GetPieceTorque(lookAheadPosition);
					
					// Weight torque more if it goes in the same direction as current angular velocity (would accelerate slipping)
					double torqueRelevance = 1;
					if (Math.Sign(carTickData.angularVelocity) * Math.Sign(torque) < 0)
						torqueRelevance = 1 / (1 + Math.Abs(carTickData.angularVelocity) * 0.25);
					
					LastLookAheadPosition = track.GetTrackTransform(lookAheadPosition);

					LastLookAheadScore = Math.Min(Math.Max(1 - Math.Abs(torque) * LookAheadTorqueWeight * torqueRelevance, 0), 1);
					// Damping with previous values
					if (lastAiTickData != null)
					{
						double dt = 1 / (double) raceAnalysis.TickRate;
						double damping = dt / (dt + LookAheadDamping);
						// Can always move down, regain score slowly
						LastLookAheadScore = Math.Min(LastLookAheadScore, lastAiTickData.lookAheadScore * (1 - damping) + LastLookAheadScore * (damping));
					}
				}

				// Evaluate time margin before crash
				{
					// How long until we reach the max slip angle
					double timeMargin = GetTimeMargin(carTickData.slipAngle, carTickData.angularVelocity);
					double maxBrakeDuration = raceAnalysis.TickRate * 1.0; // ignore anything happening over a second away 
					
					if (double.IsNaN(timeMargin) || timeMargin > maxBrakeDuration)
					{
						LastTimeMargin = maxBrakeDuration;
						LastEmergencyBrake = 0;
					}
					else
					{
						LastTimeMargin = timeMargin;
						LastEmergencyBrake = 1 - Math.Pow(timeMargin / maxBrakeDuration, SlipAngleResponse);
					}
				}

				{
					// Check track history at this point
					TrackGraph.Sample sample = track.GetClosestSample(carPosition.piecePosition);
					var maxVelocity = Math.Min(sample.maxLinearVelocity / TopSpeed, 1);
					var safety = Math.Max(1 - Math.Abs(sample.maxSlipAngle) / MaxSlipAngle, 0);
					// TODO: This is not taking into account current slip angle
					LastTrackEstimatedMaxVelocity = Math.Min(maxVelocity * (1 + safety), 1);
				}

				if (!OverrideVelocity)
				{
					// TODO: take into account turbo

					// Go at max speed;
					NextDesiredVelocity = MaxDesiredVelocity;

					// Limit desired velocity based on what we see forward
					NextDesiredVelocity = Math.Min(NextDesiredVelocity, Math.Pow(LastLookAheadScore, LookAheadResponse));

					// Ignore previous if there seems to be margin based on track history
					NextDesiredVelocity = Math.Max(NextDesiredVelocity, NextDesiredVelocity * (1 - TrackHistoryResponse) + LastTrackEstimatedMaxVelocity * TrackHistoryResponse);
					
					// Brake if we are about to crash
					NextDesiredVelocity = Math.Min(NextDesiredVelocity, 1 - LastEmergencyBrake);

					// Safety clamp
					NextDesiredVelocity = Math.Min(Math.Max(NextDesiredVelocity, 0), 1);
				}
				
				if (!OverrideThrottle)
				{
					// Compute throttle based on desired velocity
					double lastVelocity = carTickData.linearVelocity;
					double velocityDelta = NextDesiredVelocity - lastVelocity / (TopSpeed * turboFactor);
					NextDesiredThrottle = Math.Min(Math.Max(NextDesiredVelocity + velocityDelta * ThrottleAggressiveness, 0), 1);

					// Clamp
					NextDesiredThrottle = Math.Min(Math.Max(NextDesiredThrottle, 0), 1);
				}

				aiTickData.crashTimeMargin = LastTimeMargin;
				aiTickData.emergencyBrake = LastEmergencyBrake;
				aiTickData.lookAheadPosition = LastLookAheadPosition;
				aiTickData.lookAheadScore = LastLookAheadScore;
				aiTickData.estimatedTrackMaxVelocity = LastTrackEstimatedMaxVelocity;
				aiTickData.desiredThrottle = NextDesiredThrottle;
				aiTickData.desiredVelocity = NextDesiredVelocity;

				// Sanity check on speed
				if (lastAiTickData != null)
				{
					var topSpeedGivenThrottle = TopSpeed * turboFactor * lastAiTickData.desiredThrottle;
					aiTickData.expectedAcceleration = (topSpeedGivenThrottle - lastCarTickData.linearVelocity) * AccelerationStrength * turboFactor;
				}
				
				AITickData.Add(aiTickData);

				if (TurboActive)
				{
					--TurboDuration;
					TurboActive = TurboActive && TurboDuration > 0;
				}
			}

			if (!raceStatus.LastTurboActive)
				TurboActive = false;
		}

		public double GetTimeMargin(double slipAngle, double angularVelocity)
		{
			// Do not compute using acceleration, as it is damped towards 0 each frame, unless you've figured out the magic
			double slipAngleMargin = MaxSlipAngle - slipAngle * Math.Sign(angularVelocity);
			double timeMargin = slipAngleMargin / Math.Abs(angularVelocity); // in Ticks
			return timeMargin;
		}
	}
}

