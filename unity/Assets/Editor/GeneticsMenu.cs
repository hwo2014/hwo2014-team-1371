//
// GeneticsMenu.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using UnityEditor;
using AI;
using UnityEngine;
using System.Collections.Generic;
using Data;

public class GeneticsMenu : EditorWindow
{
	public Bot Bot = new Bot(true);

	public string TrackName = "keimola";
	public string Password;

	public int GenerationSize = 20;
	public float SurviveRate = 0.2f;
	public float MutateStrength = 0.15f;
	public float MutateRate = 0.3f;
	public float CombineRate = 0.3f;
	public string ReplayFolder;

	public float MaxGenerationDuration;

	public int CurrentGeneration;
	public float CurrentGenerationProgress;
	public float CurrentGenerationStartTime;

	public float BestFitness;
	public List<Bot> BestBots = new List<Bot>();

	public List<List<Bot>> Bots = new List<List<Bot>>();

	private Vector2 _scroll;

	[MenuItem ("HelloWorldOpen/Genetics...")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(GeneticsMenu));
	}
	
	public void OnGUI()
	{
		_scroll = GUILayout.BeginScrollView(_scroll);
		
		Bot.BotName = EditorGUILayout.TextField("Bot name", Bot.BotName);
		Bot.BotKey = EditorGUILayout.TextField("Bot key", Bot.BotKey);
		Bot.ServerName = EditorGUILayout.TextField("Server name", Bot.ServerName);
		int.TryParse(EditorGUILayout.TextField("Server port", Bot.ServerPort.ToString()), out Bot.ServerPort);
		TrackName = EditorGUILayout.TextField("Track name", TrackName);
		Password = EditorGUILayout.TextField("Password", Password);
		ReplayFolder = EditorGUILayout.TextField("ReplayFolder", ReplayFolder);
		GenerationSize = EditorGUILayout.IntSlider("Generation size", GenerationSize, 1, 100);
		SurviveRate = EditorGUILayout.Slider("Survive rate", SurviveRate, 0, 1);
		CombineRate = EditorGUILayout.Slider("Combine rate", CombineRate, 0, 1);
		MutateRate = EditorGUILayout.Slider("Mutate rate", MutateRate, 0, 1);
		MutateStrength = EditorGUILayout.Slider("Mutate strength", MutateStrength, 0, 1);
		MaxGenerationDuration = EditorGUILayout.Slider("Max generation duration", MaxGenerationDuration, 0, 3600);

		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Start"))
			StartGenetics();
		if (GUILayout.Button("Stop"))
			StopGenetics();
		if (GUILayout.Button("Reset"))
			Reset();
		GUILayout.EndHorizontal();
		
		EditorGUILayout.LabelField("Current generation", CurrentGeneration.ToString());
		EditorGUILayout.LabelField("Generation time", (EditorApplication.timeSinceStartup - CurrentGenerationStartTime).ToString("F2"));
		EditorGUILayout.LabelField("Generation progress", (CurrentGenerationProgress * 100).ToString("F2"));
		EditorGUILayout.LabelField("Best time", (BestFitness / 1000).ToString("F2"));
		EditorGUILayout.LabelField("Best bot", BestBots.Count > 0 ? BestBots[BestBots.Count - 1].BotName : "<N/A>");

		GUILayout.BeginVertical("Box");
		foreach (var bot in BestBots)
		{
			GUILayout.BeginHorizontal();
			EditorGUILayout.LabelField(bot.BotName, GetFitness(bot).ToString());
			GUILayout.EndHorizontal();
		}
		GUILayout.EndVertical();

		if (Bots.Count > 0)
		{
			GUILayout.BeginVertical("Box");
			var bots = Bots[Bots.Count - 1];
			foreach (var bot in bots)
			{
				GUILayout.BeginHorizontal();
				GUILayout.Label(bot.BotName, GUILayout.Width(100));
				GUILayout.Toggle(bot.Running, "Started");
				GUILayout.Toggle(bot.Ready, "Ready");
				GUILayout.Toggle(bot.RaceStatus.Initialized, "Playing");
				GUILayout.Toggle(bot.Completed, "Completed");
				GUILayout.Label(String.Format("[{0:F2}%]", bot.RaceStatus.Initialized ? bot.RaceAnalysis.RaceProgress * 100 : 0));
				GUILayout.EndHorizontal();
			}
			GUILayout.EndVertical();
		}

		GUILayout.EndScrollView();
	}

	public void StartGenetics()
	{
		CurrentGeneration = 1;
		List<Bot> newBots = new List<Bot>();
		for (int i = 0; i < GenerationSize; ++i)
		{
			var bot = new Bot(Bot);
			bot.RaceAI = Mutate(Bot.RaceAI);
			bot.BotName = String.Format("{0}_{1}_{2}", Bot.BotName, CurrentGeneration, newBots.Count);
			bot.CreateRace(TrackName, Password, 1);
			newBots.Add(bot);
		}
		Bots.Add(newBots);
		CurrentGenerationProgress = 0;
		CurrentGenerationStartTime = (float) EditorApplication.timeSinceStartup;
		BestBots.Clear();
		BestFitness = float.MaxValue;
	}

	public RaceAI Mutate(RaceAI ai)
	{
		var newAI = new RaceAI(ai);

		newAI.ThrottleAggressiveness *= (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength));
		newAI.LookAheadTime *= (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength));
		newAI.LookAheadDamping *= (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength));
		newAI.LookAheadTorqueWeight *= (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength));
		newAI.LookAheadResponse *= (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength));
		newAI.TrackHistoryResponse *= (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength));
		newAI.SlipAngleResponse *= (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength));
		newAI.MaxTimeMargin = Mathf.RoundToInt((float) newAI.MaxTimeMargin * (1 + UnityEngine.Random.Range(-MutateStrength, MutateStrength)));

		return newAI;
	}

	public RaceAI Combine(RaceAI ai1, RaceAI ai2)
	{
		var newAI = new RaceAI(ai1);

		newAI.ThrottleAggressiveness *= UnityEngine.Random.value > 0.5 ? ai1.ThrottleAggressiveness : ai2.ThrottleAggressiveness;
		newAI.LookAheadTime *= UnityEngine.Random.value > 0.5 ? ai1.LookAheadTime : ai2.LookAheadTime;
		newAI.LookAheadDamping *= UnityEngine.Random.value > 0.5 ? ai1.LookAheadDamping : ai2.LookAheadDamping;
		newAI.LookAheadTorqueWeight *= UnityEngine.Random.value > 0.5 ? ai1.LookAheadTorqueWeight : ai2.LookAheadTorqueWeight;
		newAI.LookAheadResponse *= UnityEngine.Random.value > 0.5 ? ai1.LookAheadResponse : ai2.LookAheadResponse;
		newAI.TrackHistoryResponse *= UnityEngine.Random.value > 0.5 ? ai1.TrackHistoryResponse : ai2.TrackHistoryResponse;
		newAI.SlipAngleResponse *= UnityEngine.Random.value > 0.5 ? ai1.SlipAngleResponse : ai2.SlipAngleResponse;
		newAI.MaxTimeMargin *= UnityEngine.Random.value > 0.5 ? ai1.MaxTimeMargin : ai2.MaxTimeMargin;

		return newAI;
	}
	
	public void StopGenetics()
	{
		if (CurrentGeneration > 0)
			StopCurrentGeneration();
	}
	
	public void Reset()
	{
		StopGenetics();
		CurrentGeneration = 0;
		Bots.Clear();
	}

	public static float GetFitness(Bot bot)
	{
		if (bot.RaceStatus.GameEnd == null || bot.RaceStatus.GameEnd.bestLaps == null)
			return float.MaxValue;
		return bot.RaceStatus.GameEnd.bestLaps[0].result.millis;
	}

	void StartNextGeneration()
	{
		CurrentGeneration += 1;
		var lastBots = Bots[Bots.Count - 1];
		lastBots.Sort((a, b) => GeneticsMenu.GetFitness(a).CompareTo(GeneticsMenu.GetFitness(b)));
		int surviveCount = Mathf.FloorToInt(GenerationSize * SurviveRate);
		int mutateCount = Mathf.FloorToInt(surviveCount + GenerationSize * MutateRate);
		int combineCount = Mathf.FloorToInt(mutateCount + GenerationSize * CombineRate);

		var newBots = new List<Bot>();
		while (newBots.Count < surviveCount)
		{
			var newBot = new Bot(lastBots[newBots.Count]);
			newBot.BotName = String.Format("{0}_{1}_{2}", Bot.BotName, CurrentGeneration, newBots.Count);
			newBots.Add(newBot);
		}

		while (newBots.Count < mutateCount)
		{
			var oldBot = lastBots[UnityEngine.Random.Range(0, surviveCount)];
			var newBot = new Bot(oldBot);
			newBot.BotName = String.Format("{0}_{1}_{2}", Bot.BotName, CurrentGeneration, newBots.Count);
			newBot.RaceAI = Mutate(oldBot.RaceAI);
			newBots.Add(newBot);
		}

		while (newBots.Count < combineCount)
		{
			var oldBot1 = lastBots[UnityEngine.Random.Range(0, surviveCount)];
			var oldBot2 = lastBots[UnityEngine.Random.Range(0, surviveCount)];
			while (oldBot2 == oldBot1)
				oldBot2 = lastBots[UnityEngine.Random.Range(0, surviveCount)];

			var newBot = new Bot(oldBot1);
			newBot.BotName = String.Format("{0}_{1}_{2}", Bot.BotName, CurrentGeneration, newBots.Count);
			newBot.RaceAI = Combine(oldBot1.RaceAI, oldBot2.RaceAI);
			newBots.Add(newBot);
		}

		while (newBots.Count < combineCount)
		{
			var otherBot = newBots[UnityEngine.Random.Range(0, newBots.Count)];
			var newBot = new Bot(otherBot);
			newBot.BotName = String.Format("{0}_{1}_{2}", Bot.BotName, CurrentGeneration, newBots.Count);
			newBot.RaceAI = Mutate(otherBot.RaceAI);
			newBots.Add(newBot);
		}

		foreach (var bot in newBots)
			bot.CreateRace(TrackName, Password, 1);

		Bots.Add(newBots);

		CurrentGenerationProgress = 0;
		CurrentGenerationStartTime = (float) EditorApplication.timeSinceStartup;
	}

	void StopCurrentGeneration()
	{
		float bestFitness = float.MaxValue;
		Bot bestBot = null;
		foreach (var bot in Bots[Bots.Count - 1])
		{
			bot.Stop();
			var gameObject = new GameObject(bot.BotName);
			var replay = gameObject.AddComponent<ReplayBehavior>();
			replay.Bot = bot;
			PrefabUtility.CreatePrefab("Assets/Replays/" + ReplayFolder + "/" + bot.BotName + ".prefab", gameObject, ReplacePrefabOptions.ReplaceNameBased);
			DestroyImmediate(gameObject);
			float fitness = GetFitness(bot);
			if (fitness < bestFitness)
			{
				bestFitness = fitness;
				bestBot = bot;
			}
		}

		if (bestFitness < BestFitness)
		{
			BestFitness = bestFitness;
			BestBots.Add(bestBot);
		}

		// Save prefabs
		EditorApplication.SaveScene();
	}
		
	void Update()
	{
		if (Bots.Count > 0)
		{
			int completed = 0;
			float minProgress = float.MaxValue;
			foreach (var bot in Bots[Bots.Count - 1])
			{
				if (bot.Completed)
					completed++;
				minProgress = Mathf.Min(minProgress, (float) bot.RaceAnalysis.RaceProgress);
			}
			CurrentGenerationProgress = minProgress;
			if (completed == GenerationSize || EditorApplication.timeSinceStartup - CurrentGenerationStartTime > MaxGenerationDuration)
			{
				StopCurrentGeneration();
				StartNextGeneration();
			}
		}

		Repaint();
	}
}

