//
// MultipleBotsMenu.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Messages;
using UnityEditor;
using UnityEngine;
using Data;
using AI;
using System.Collections.Generic;
using System.Threading;

public class MultipleBotsMenu : EditorWindow
{
	public List<Bot> Bots = new List<Bot>();

	private Vector2 _scroll;

	[MenuItem ("HelloWorldOpen/Multiple bots...")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(MultipleBotsMenu));
	}
	
	public void OnGUI()
	{
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Fill race"))
		{
			FillRace();
		}

		if (GUILayout.Button("Reset"))
		{
			ResetBots();
		}
		GUILayout.EndHorizontal();
		    
		_scroll = GUILayout.BeginScrollView(_scroll);

		foreach (Bot bot in Bots)
		{
			GUILayout.BeginHorizontal("Box");
			GUILayout.Label(bot.BotName, GUILayout.Width(80));
			GUILayout.Toggle(bot.Running, "S");
			GUILayout.Toggle(bot.Ready, "R");
			GUILayout.Toggle(bot.RaceStatus.Initialized, "P");
			GUILayout.Label(String.Format("[{0:F2}%]", bot.RaceStatus.Initialized ? bot.RaceAnalysis.RaceProgress * 100 : 0));
			GUILayout.EndHorizontal();
			bot.RaceAI.MaxDesiredVelocity = (double) Mathf.Clamp01(EditorGUILayout.Slider("Max Velocity", (float) bot.RaceAI.MaxDesiredVelocity, 0, 1));
		}

		GUILayout.EndScrollView();
	}

	public void ResetBots()
	{
		foreach (Bot bot in Bots)
		{
			bot.Stop();
		}
		Bots.Clear();
	}
	
	public void FillRace()
	{
		ResetBots();
		SingleBotMenu mainMenu = GetWindow<SingleBotMenu>();

		for (int i = 0; i < mainMenu.CarCount - 1; ++i)
		{
			Bot newBot = new Bot(mainMenu.Bot);
			newBot.BotName = newBot.BotName + "_" + (i + 1).ToString();
			Bots.Add(newBot);
			newBot.JoinRace(mainMenu.TrackName, mainMenu.Password, mainMenu.CarCount);
			for (int n = 0; n < 10; ++n)
			{
				if (newBot.Ready)
					break;
				Thread.Sleep(100); // Some bots fail to join otherwise
			}
		}
	}

	public void Update()
	{
		if (Bots.Count == 0)
			return;

		var currentRaceObject = GameObject.Find("CurrentRace");
		if (currentRaceObject != null)
		{
			// Fill out car IDs.
			var replay = currentRaceObject.GetComponent<ReplayBehavior>();
			if (replay == null || replay.Cars == null)
				return;

			foreach (Transform carTransform in replay.Cars)
			{
				CarBehavior car = carTransform.gameObject.GetComponent<CarBehavior>();
				if (car.RaceAI == null)
				{
					foreach (Bot bot in Bots)
					{
						if (bot.RaceStatus.OwnCarListIndex == car.CarListIndex)
						{
							car.RaceAI = bot.RaceAI;
							car.RaceAnalysis = bot.RaceAnalysis;
							car.RaceStatus = bot.RaceStatus;
						}
					}
				}
			}
		}

		Repaint();
	}
}
