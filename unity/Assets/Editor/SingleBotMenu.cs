﻿//
// SingleBotMenu.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using Messages;
using UnityEditor;
using UnityEngine;
using Data;
using AI;

public class SingleBotMenu : EditorWindow
{
	public Bot Bot = new Bot(true);
	public ReplayBehavior CurrentRace;

	public string TrackName = "keimola";
	public string Password;
	public int CarCount;

	public string Status;

	private Vector2 _scroll;
	private int _displayedMsgId;
	private Msg _displayedMsg;
	private int _displayedLogCount;

	[MenuItem ("HelloWorldOpen/Single bot...")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(SingleBotMenu));
	}

	public void OnGUI()
	{
		_scroll = GUILayout.BeginScrollView(_scroll);

		GUI.enabled = !Bot.Running;
		Bot.BotName = EditorGUILayout.TextField("Bot name", Bot.BotName);
		Bot.BotKey = EditorGUILayout.TextField("Bot key", Bot.BotKey);
		Bot.ServerName = EditorGUILayout.TextField("Server name", Bot.ServerName);
		int.TryParse(EditorGUILayout.TextField("Server port", Bot.ServerPort.ToString()), out Bot.ServerPort);
		TrackName = EditorGUILayout.TextField("Track name", TrackName);
		Password = EditorGUILayout.TextField("Password", Password);
		CarCount = EditorGUILayout.IntSlider("Car count", CarCount, 1, 50); 

		GUILayout.BeginHorizontal();
		GUI.enabled = !Bot.Running;
		if (GUILayout.Button("Quick"))
			QuickRace();
		GUI.enabled = !Bot.Running;
		if (GUILayout.Button("Create"))
			CreateRace();
		GUI.enabled = !Bot.Running;
		if (GUILayout.Button("Join"))
			JoinRace();
		GUI.enabled = Bot.Running;
		if (GUILayout.Button("Stop"))
			StopBot();
		GUI.enabled = true;
		if (GUILayout.Button("Reset"))
		{
			Bot.Stop();
			ResetBot();
		}
		GUILayout.EndHorizontal();

		GUI.enabled = true;
		Bot.AITimeout = EditorGUILayout.IntSlider("AI timeout (ms)", Bot.AITimeout, 0, 250);
		Bot.AICheckInterval = EditorGUILayout.IntSlider("AI check interval (ms)", Bot.AICheckInterval, 0, 25);
		Bot.AISleepDuration = EditorGUILayout.IntSlider("AI sleep duration (ms)", Bot.AISleepDuration, 0, 25);
		EditorGUILayout.LabelField("AI ticks behind", Bot.TicksBehind.ToString());
		EditorGUILayout.LabelField("Time/message (ms)", Bot.AverageTimePerAIMessageSent.ToString("F2"));
		EditorGUILayout.LabelField("Time/update (ms)", Bot.AverageTimePerAIUpdate.ToString("F2"));
		GUI.enabled = true;

		var raceStatus = Bot.RaceStatus;
		var raceAnalysis = Bot.RaceAnalysis;

		CarId id = raceStatus.OwnCarId;
		if (id != null)
		{
			GUILayout.Label("Car:");
			GUILayout.BeginVertical("Box");
			EditorGUILayout.LabelField("Name", id.name);
			EditorGUILayout.LabelField("Color", id.color);
			GUILayout.EndVertical();
		}

		GUILayout.Label("Status:");
		GUILayout.BeginVertical("Box");
		EditorGUILayout.TextField("Game ID", raceStatus.GameId);
		EditorGUILayout.LabelField("Last game tick", raceStatus.LastGameTick.ToString());
		EditorGUILayout.HelpBox(Status, MessageType.None);
		EditorGUI.ProgressBar(GUILayoutUtility.GetLastRect(), (float) raceAnalysis.RaceProgress, Status);
		GUILayout.EndVertical();

		GUILayout.Label("Messages:");
		GUILayout.BeginVertical("Box");
		EditorGUILayout.LabelField("Sent", Bot.SentCount.ToString());
		EditorGUILayout.LabelField("Received", Bot.ReceivedCount.ToString());
		EditorGUILayout.LabelField("Total", (Bot.SentCount + Bot.ReceivedCount).ToString());
		GUILayout.EndVertical();

		GUI.changed = false;
		int count = Bot.Messages.Count;
		_displayedMsgId = EditorGUILayout.IntSlider("Display", _displayedMsgId, 0, Math.Max(0, count - 1));
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Start"))
			_displayedMsgId = 0;
		if (GUILayout.Button("-1"))
			_displayedMsgId -= 1;
		if (GUILayout.Button("+1"))
			_displayedMsgId += 1;
		if (GUILayout.Button("End"))
			_displayedMsgId = (Bot.SentCount + Bot.ReceivedCount) - 1;
		_displayedMsgId = Mathf.RoundToInt(Mathf.Clamp(_displayedMsgId, 0, Math.Max(0, count - 1)));
		GUILayout.EndHorizontal();

		if (GUI.changed)
			_displayedMsg = _displayedMsgId < Bot.Messages.Count ? Bot.Messages[_displayedMsgId] : null;

		string message = _displayedMsg != null ? _displayedMsg.MsgDirection() + " ->\n" + _displayedMsg.ToJson(true, false) : "<No message>";
		GUILayout.TextArea(message);

		GUILayout.EndScrollView();
	}

	public void QuickRace()
	{
		PrepareBot();
		Bot.QuickRace();
	}

	public void CreateRace()
	{
		PrepareBot();
		Bot.CreateRace(TrackName, Password, CarCount);
	}

	public void JoinRace()
	{
		PrepareBot();
		Bot.JoinRace(TrackName, Password, CarCount);
	}

	public void ResetBot()
	{
		Bot = new Bot(Bot);
		GetWindow<RaceAIMenu>().Bot = Bot;
	}
	
	private void PrepareBot()
	{
		_displayedLogCount = 0;
		_displayedMsg = null;
		_displayedMsgId = 0;

		var replays = Resources.FindObjectsOfTypeAll(typeof(ReplayBehavior)) as ReplayBehavior[];
		foreach (var replay in replays)
		{
			if (replay.name == "CurrentRace")
				DestroyImmediate(CurrentRace.gameObject);
		}
			
		var raceObject = new GameObject("CurrentRace");
		CurrentRace = raceObject.AddComponent<ReplayBehavior>();

		if (Bot.Running)
			Bot.Stop();
		
		// Hard reset bot
		ResetBot();
		
		CurrentRace.Bot = Bot;
		CurrentRace.Clear();
		GetWindow<ReplayMenu>().Replay = CurrentRace;
		GetWindow<RaceAIMenu>().Bot = Bot;
		Selection.activeGameObject = null;
	}

	public void StopBot()
	{
		Bot.Stop();

		var raceStatus = Bot.RaceStatus;

		// Create replay object
		if (raceStatus.GameInit != null && raceStatus.OwnCarId != null)
		{
			string name = String.Format("{0:yy-MM-dd-H-mm-ss}_{1}_{2}_{3:F2}s",
			                            DateTime.Now,
			                            raceStatus.GameInit.race.track.id,
			                            raceStatus.OwnCarId.name,
			                            raceStatus.OwnResult != null ? raceStatus.OwnResult.result.millis / 1000f : 0);
			var replayContainer = new GameObject(name);
			var replay = replayContainer.AddComponent<ReplayBehavior>();
			replay.Bot = Bot;
			replay.Rebuild(true);

			GetWindow<ReplayMenu>().Replay = replay;
			GetWindow<ReplayMenu>().Playing = false;
			Selection.activeGameObject = null;
		}

		if (CurrentRace != null)
			DestroyImmediate(CurrentRace.gameObject);
	}

	void Update()
	{
		// Keep up with log
		lock (Bot.Log)
		{
			while (_displayedLogCount < Bot.Log.Count)
			{
				Status = Bot.Log[_displayedLogCount];
				Debug.Log(Status);
				Status = Status.Substring(0, Math.Min(Status.Length, 80));
				++_displayedLogCount;
			}
		}
		if (Bot.RequestingAbort && Bot.Running)
			StopBot();
		Repaint();
	}
}
