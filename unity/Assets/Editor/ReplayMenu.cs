//
// ReplayMenu.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System;
using System.Collections.Generic;
using AI;
using Data;
using Messages;
using UnityEditor;
using UnityEngine;

public class ReplayMenu : EditorWindow
{
	public ReplayBehavior Replay
	{
		get { return _replay; }
		set
		{
			if (_replay == value)
				return;
			if (value != null && EditorUtility.IsPersistent(value.gameObject))
				return;
			var allReplays = FindObjectsOfType<ReplayBehavior>();
			foreach (var replay in allReplays)
				replay.gameObject.SetActive(false);
			_replay = value;
			_displayedTick = 0;
			if (_replay != null)
			{
				_replay.gameObject.SetActive(true);
				_replay.SetTick(0);
				_replay.Rebuild(false);
				Playing = true;
				_playStartTime = (float) EditorApplication.timeSinceStartup;
				_playStartTick = _displayedTick;
			}
			Repaint();
		}
	}

	public bool CopyScaledData = true;
	public bool CopyHeader = false;
	public bool CopyOnlyDisplayed = true;
	public bool Playing;

	private ReplayBehavior _replay;
	private Vector2 _scroll;
	private int _displayedTick;
	private int _tickRate = 60;
	private float _playStartTime;
	private int _playStartTick;
	private bool _selectActiveCar;

	public static GUIStyle ValueStyle;
	
	[MenuItem ("HelloWorldOpen/Replays...")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(ReplayMenu));
	}

	public void OnSelectionChange()
	{
		var gameObjects = Selection.gameObjects;
		foreach (var gameObject in gameObjects)
		{
			var obj = gameObject;
			while (true)
			{
				CarBehavior car = obj.GetComponent<CarBehavior>();
				if (car != null && Replay != null)
				{
					Replay.ActiveCar = car;
				}
				ReplayBehavior replay = obj.GetComponent<ReplayBehavior>();
				if (replay != null)
				{
					Replay = replay;
					return;
				}
				if (obj.transform.parent == null)
					return;
				obj = obj.transform.parent.gameObject;
			}
		}
	}
	
	public void OnGUI()
	{
		Replay = EditorGUILayout.ObjectField("Replay", Replay, typeof(ReplayBehavior), true) as ReplayBehavior;
		GUI.enabled = Replay != null;
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Rebuild"))
		{
			Replay.Rebuild(true);
		}
		if (GUILayout.Button("Clear and save"))
		{
			Replay.Clear();
			PrefabUtility.CreatePrefab("Assets/Replays/" + Replay.name + ".prefab", Replay.gameObject, ReplacePrefabOptions.ReplaceNameBased);
		}
		GUILayout.EndHorizontal();
		GUI.enabled = true;
		if (Replay == null)
		{
			EditorGUILayout.HelpBox("Select a replay to begin", MessageType.Info);
			return;
		}

		ReplayBehavior replay = Replay;
		RaceStatus raceStatus = replay.Bot.RaceStatus;
		if (raceStatus == null)
		{
			EditorGUILayout.HelpBox("Replay does not contain RaceStatus", MessageType.Error);
			return;
		}
		RaceAnalysis raceAnalysis = replay.Bot.RaceAnalysis;
		if (raceAnalysis == null)
		{
			EditorGUILayout.HelpBox("Replay does not contain RaceAnalysis", MessageType.Error);
			return;
		}

		if (!raceStatus.Initialized)
		{
			EditorGUILayout.HelpBox("Race doesn't have init data", MessageType.Error);
			return;
		}
		
		_scroll = GUILayout.BeginScrollView(_scroll);
		GameInit gameInit = raceStatus.GameInit;

		if (gameInit != null)
		{
			GUILayout.Label("Game:");
			GUILayout.BeginVertical("Box");
			EditorGUILayout.TextField("Game ID", replay.GameId);
			EditorGUILayout.TextField("Track", gameInit.race.track.id);
			EditorGUILayout.TextField("Laps", gameInit.race.raceSession.laps.ToString());
			EditorGUILayout.TextField("Cars", gameInit.race.cars.Length.ToString());
			EditorGUILayout.TextField("Quick race", gameInit.race.raceSession.quickRace.ToString());
			GUILayout.EndVertical();
		}

		List<CarPositions> positions = raceStatus.CarPositions;
		int maxTick = Math.Max(0, positions.Count - 1);
		if (positions != null)
		{
			GUILayout.Label("Replay:");
			GUILayout.BeginVertical("Box");
			GUI.changed = false;
			_displayedTick = replay.DisplayedTick;
			_displayedTick = EditorGUILayout.IntSlider("Tick", _displayedTick, 0, maxTick);
			if (GUI.changed)
				Playing = false;
			_tickRate = EditorGUILayout.IntSlider("Tick rate", _tickRate, -300, 300);
			GUILayout.BeginHorizontal();
			GUI.enabled = !Playing;
			if (GUILayout.Button("Play"))
			{
				Playing = true;
			}
			GUI.enabled = Playing;
			if (GUILayout.Button("Stop"))
			{
				Playing = false;
			}
			GUILayout.EndHorizontal();
			GUI.enabled = true;

			GUILayout.BeginHorizontal();
			if (GUILayout.Button("0"))
				_displayedTick = 0;
			if (GUILayout.Button("-1"))
				_displayedTick -= 1;
			if (GUILayout.Button("+1"))
				_displayedTick += 1;
			if (GUILayout.Button(maxTick.ToString()))
				_displayedTick = maxTick;
			_displayedTick = Mathf.RoundToInt(Mathf.Clamp(_displayedTick, 0, maxTick));
			GUILayout.EndHorizontal();

			if (GUI.changed)
			{
				replay.SetTick(_displayedTick);
				if (Replay.ActiveCar != null)
					EditorUtility.SetDirty(Replay.ActiveCar);
				if (Playing)
				{
					_playStartTime = (float) EditorApplication.timeSinceStartup;
					_playStartTick = _displayedTick;
				}
			}
			GUILayout.EndVertical();
		}

		if (GUI.changed)
			SceneView.RepaintAll();

		GUILayout.Space(10);
		
		CarBehavior car = Replay.ActiveCar;
		if (car != null)
		{
			GUILayout.Label("Selected car:");

			GUILayout.BeginVertical("Box");

			var carPosition = car.Replay.DisplayedTick < car.RaceStatus.CarPositions.Count ? car.RaceStatus.CarPositions[car.Replay.DisplayedTick].positions[car.CarListIndex] : null;
			var carTickDataArray = car.Replay.DisplayedTick < car.RaceAnalysis.CarTickData.Count ? car.RaceAnalysis.CarTickData[car.Replay.DisplayedTick] : null;
			var aiTickData = car.RaceAI != null && car.Replay.DisplayedTick < car.RaceAI.AITickData.Count ? car.RaceAI.AITickData[car.Replay.DisplayedTick] : null;
			if (carTickDataArray != null && carPosition != null)
			{
				ValueStyle = new GUIStyle(GUI.skin.GetStyle("Label"));
				ValueStyle.alignment = TextAnchor.MiddleRight;
				var carTickData = carTickDataArray.tickData[car.CarListIndex];
				GUILayout.Label(car.CarId.name);

				GUILayout.Button("Lap Progress");
				var lapProgress = car.RaceStatus.GameInit.race.track.GetLapProgress(carPosition);
				EditorGUI.ProgressBar(GUILayoutUtility.GetLastRect(), (float) lapProgress,
				                      String.Format("Lap {0}/{1}", carPosition.piecePosition.lap + 1,
				              car.RaceStatus.GameInit.race.raceSession.laps));
				
				int i = 0;
				foreach (var lap in raceStatus.LapsFinished)
				{
					if (!lap.car.SameCar(car.CarId))
						continue;
					++i;
					GUILayout.BeginHorizontal();
					GUILayout.Label(String.Format("Lap {0}", i), GUILayout.Width(50));
					GUILayout.Space(10);
					GUILayout.Label(String.Format("{0:F2}s", lap.lapTime.millis / 1000.0), ValueStyle, GUILayout.Width(50));
					GUILayout.Label(String.Format("#{0}", lap.ranking.fastestLap), GUILayout.Width(30));
					GUILayout.Space(10);
					GUILayout.Label(String.Format("{0:F2}s", lap.raceTime.millis / 1000.0), ValueStyle, GUILayout.Width(50));
					GUILayout.Label(String.Format("#{0}", lap.ranking.overall), GUILayout.Width(30));
					GUILayout.EndHorizontal();
				}
				while (i < raceStatus.GameInit.race.raceSession.laps)
				{
					++i;
					GUILayout.BeginHorizontal();
					GUILayout.Label(String.Format("Lap {0}", i), GUILayout.Width(50));
					GUILayout.Space(10);
					GUILayout.Label("<No data>");
					GUILayout.EndHorizontal();
				}
				
				TrackTransformBoxHeader();
				TrackTransformBox("Car position", carTickData.position);
				if (aiTickData != null)
				{
					TrackTransformBox("Lookahead position", aiTickData.lookAheadPosition);
				}
				var carProperties = PreferenceBehavior.Instance.CarProperties;
				foreach (CarBehavior.CarProperty property in carProperties)
				{
					PropertyBox(property, carTickData, aiTickData);
				}
				EditorGUILayout.Toggle("Data was available", carTickData.available);

				GUILayout.BeginHorizontal();
				if (Replay.ActiveCar != null && GUILayout.Button("Copy CSV"))
				{
					GenerateCSV(Replay.ActiveCar.GetComponent<CarBehavior>());
				}
				CopyScaledData = GUILayout.Toggle(CopyScaledData, "Scaled");
				CopyHeader = GUILayout.Toggle(CopyHeader, "Header");
				CopyOnlyDisplayed = GUILayout.Toggle(CopyOnlyDisplayed, "Only display range");
				GUILayout.EndHorizontal();

				PreferenceBehavior.Instance.DisplayRange = EditorGUILayout.IntSlider("Display range", PreferenceBehavior.Instance.DisplayRange, 0, 1200);
			
				if (GUILayout.Button("Generate curves"))
				{
					GenerateAnimationCurves(car);
				}
				if (GUILayout.Button("Wipe & recompute"))
				{
					Recompute(car);
				}

				GUILayout.EndVertical();
			}
		}

		GUILayout.EndScrollView();

		
		if (GUI.changed)
			SceneView.RepaintAll();
	}

	public void Recompute(CarBehavior car)
	{
		lock (car.RaceStatus)
		{
			car.RaceStatus.GameInit.race.track.trackGraph.ClearSamples();
			car.RaceAnalysis.CarTickData.Clear();
			car.RaceAnalysis.ComputeCarTickData(car.RaceStatus);
			car.RaceAI.AITickData.Clear();
			car.RaceAI.ComputeDesiredThrottle(car.RaceStatus, car.RaceAnalysis);
		}
		SceneView.RepaintAll();
	}
	
	public void GenerateAnimationCurves(CarBehavior car)
	{
		DestroyImmediate(car.gameObject.GetComponent<Animation>());
		var animation = car.gameObject.AddComponent<Animation>();
		animation.AddClip(new AnimationClip(), "CarData");
		var clip = animation.GetClip("CarData");

		var carProperties = PreferenceBehavior.Instance.CarProperties;
		foreach (CarBehavior.CarProperty property in carProperties)
		{
			var curve = new AnimationCurve();
			for (int i = 0; i < car.RaceAnalysis.CarTickData.Count; ++i)
			{
				var carTickData = car.RaceAnalysis.CarTickData[i].tickData[car.CarListIndex];
				var aiTickData = car.RaceAI != null && i < car.RaceAI.AITickData.Count ? car.RaceAI.AITickData[i] : null;
				curve.AddKey(i, (float) (property.Scale * property.Value(carTickData, aiTickData)));
			}
			clip.SetCurve("", typeof(CarBehavior), property.Name, curve);
		}

		var tickCurve = new AnimationCurve();
		for (int i = 0; i < car.RaceAnalysis.CarTickData.Count; ++i)
		{
			tickCurve.AddKey(i, i);
		}
		clip.SetCurve("", typeof(CarBehavior), "ReplayTick", tickCurve);
			
		animation.playAutomatically = false;
		Selection.activeGameObject = null;
	}

	static double NiceNumber(int steps)
	{
		int pow = steps / 9;
		double offset = Math.Pow(10, pow - 2);
		steps = steps - pow * 9;
		return offset * (steps + 1);
	}

	static int NiceNumberSteps(double number)
	{
		int pow = 0;
		while (Math.Pow(10, pow - 2) < number)
		{
			++pow;
		}
		int steps = (pow - 1) * 9;
		double offset = Math.Pow(10, pow - 3);
		return steps + (int) Math.Round(number / offset) - 1;
	}
	
	private static void PropertyBox(CarBehavior.CarProperty property, CarTickData carTickData, AITickData aiTickData)
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label(property.Name, GUILayout.Width(110));
		double value = property.Value(carTickData, aiTickData);
		GUILayout.Label(value.ToString("F3"), ValueStyle, GUILayout.Width(50));
		//GUILayout.Label("x", GUILayout.Width(10));
		property.Scale = EditorGUILayout.FloatField((float) property.Scale, GUILayout.Width(40));
		property.Scale = NiceNumber(Mathf.FloorToInt(GUILayout.HorizontalSlider((float) NiceNumberSteps(property.Scale), -1, 55, GUILayout.Width(130))));
		value *= property.Scale;
		//GUILayout.Label("=", GUILayout.Width(10));
		//GUILayout.Label(value.ToString("F3"), ValueStyle, GUILayout.Width(50));
		property.ShowGizmo = GUILayout.Toggle(property.ShowGizmo, "", "Button", GUILayout.Width(20));
		property.Color = EditorGUILayout.ColorField(property.Color);
		GUILayout.EndHorizontal();
	}
	
	private static void TrackTransformBox(string name, TrackTransform trackTransform)
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label(name, GUILayout.Width(120));
		GUILayout.Label(trackTransform.position.x.ToString("F2"), ValueStyle, GUILayout.Width(50));
		GUILayout.Label(trackTransform.position.y.ToString("F2"), ValueStyle, GUILayout.Width(50));
		GUILayout.Label(trackTransform.angle.ToString("F2"), ValueStyle, GUILayout.Width(50));
		GUILayout.Label(trackTransform.position.magnitude.ToString("F2"), ValueStyle, GUILayout.Width(50));
		GUILayout.EndHorizontal();
	}
	private static void TrackTransformBoxHeader()
	{
		GUILayout.BeginHorizontal();
		GUILayout.Label("", GUILayout.Width(120));
		GUILayout.Label("x", ValueStyle, GUILayout.Width(50));
		GUILayout.Label("y", ValueStyle, GUILayout.Width(50));
		GUILayout.Label("angle", ValueStyle, GUILayout.Width(50));
		GUILayout.Label("length", ValueStyle, GUILayout.Width(50));
		GUILayout.EndHorizontal();
	}

	void GenerateCSV(CarBehavior car)
	{
		string csv = "";
		string sep = "\t";
		var carProperties = PreferenceBehavior.Instance.CarProperties;
		
		lock (car.RaceStatus)
		{
			if (CopyHeader)
			{
				// header
				foreach (CarBehavior.CarProperty property in carProperties)
				{
					csv += property.Name + sep;
	            }
	            csv += "\n";
			}

			int min = 0;
			int max = car.RaceAnalysis.CarTickData.Count - 1;
			if (CopyOnlyDisplayed)
			{
				min = Mathf.Max(0, Replay.DisplayedTick - PreferenceBehavior.Instance.DisplayRange);
				max = Replay.DisplayedTick;
			}
			for (int i = min; i < max; ++i)
			{
				CarTickData carTickData = car.RaceAnalysis.CarTickData[i].tickData[car.CarListIndex];
				AITickData aiTickData = i < car.RaceAI.AITickData.Count ? car.RaceAI.AITickData[i] : new AITickData();
				foreach (CarBehavior.CarProperty property in carProperties)
				{
					csv += (property.Value(carTickData, aiTickData) * (CopyScaledData ? property.Scale : 1)).ToString("F8") + sep;
				}
				csv += "\n";
			}
		}
		EditorGUIUtility.systemCopyBuffer = csv;
	}


	void Update()
	{
		if (Replay == null)
		{
			Playing = false;
			return;
		}

		Replay.Rebuild(false);

		// Hack: hide track wireframe
		{
			foreach (Transform piece in Replay.Pieces)
			{
				EditorUtility.SetSelectedWireframeHidden(piece.GetComponent<MeshRenderer>(), true);
			}
		}

		var raceStatus = Replay.Bot.RaceStatus;
		if (raceStatus == null)
			return;

		if (Playing)
		{
			float time = (float) EditorApplication.timeSinceStartup;
			float deltaTime = time - _playStartTime;
			int deltaTick = Mathf.RoundToInt(deltaTime * _tickRate);
			_displayedTick += deltaTick;
			_playStartTime += (float) deltaTick / _tickRate;
			_playStartTick += deltaTick;

			if (Replay.Bot.Running)
				_displayedTick = Mathf.Max(raceStatus.CarPositions.Count - 1, 0);
			else
				_displayedTick = _displayedTick % Mathf.Max(raceStatus.CarPositions.Count - 1, 1);

			Replay.SetTick(_displayedTick);

			Repaint();
			SceneView.RepaintAll();
		}

		if (Replay.DisplayedTick != _displayedTick)
		{
			_displayedTick = Replay.DisplayedTick;
			Repaint();
		}

		if (Replay.ActiveCar == null && Replay.Cars != null && Replay.Cars.childCount > 0)
		{
			Replay.ActiveCar = Replay.Cars.GetChild(0).GetComponent<CarBehavior>();
		}
		
		if (Replay.ActiveCar != null)
		{
			var car = Replay.ActiveCar;
			if (car.ReplayTick != car.LastReplayTick)
			{
				car.Replay.SetTick(Mathf.RoundToInt(car.ReplayTick));
				car.LastReplayTick = car.ReplayTick;
				SceneView.RepaintAll();
			}
		}
		
		// Auto select first car?
		if (Selection.activeGameObject == null && Replay.ActiveCar)
		{  
			if (_selectActiveCar)
				Selection.activeGameObject = Replay.ActiveCar.gameObject;
			_selectActiveCar = true;
		}
		else
		{
			_selectActiveCar = false;
		}
	}
}