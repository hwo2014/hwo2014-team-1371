//
// RaceAIMenu.cs
//
// Author:
//       Florent D'Halluin <florent.dhalluin@gmail.com>
//
// Copyright (c) 2014 Florent D'Halluin
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
using System;
using System.Collections.Generic;
using AI;
using Data;
using Messages;
using UnityEditor;
using UnityEngine;

public class RaceAIMenu : EditorWindow
{
	public Bot Bot;

	private Vector2 _scroll;

	[MenuItem ("HelloWorldOpen/AI viewer...")]
	static void Init()
	{
		EditorWindow.GetWindow(typeof(RaceAIMenu));
	}
	
/*	public void OnSelectionChange()
	{
		var gameObjects = Selection.gameObjects;
		foreach (var gameObject in gameObjects)
		{
			var obj = gameObject;
			while (true)
			{
				ReplayBehavior replay = obj.GetComponent<ReplayBehavior>();
				if (replay != null)
				{
					Bot = replay.Bot;
					return;
				}
				if (obj.transform.parent == null)
					return;
				obj = obj.transform.parent.gameObject;
			}
		}
	}
*/	
	public void OnGUI()
	{
		GUILayout.BeginHorizontal();
		if (GUILayout.Button("Select main bot"))
		{
			Bot = GetWindow<SingleBotMenu>().Bot;
		}
		if (GUILayout.Button("Select replay bot"))
		{
			Bot = GetWindow<ReplayMenu>().Replay.Bot;
		}
		GUILayout.EndHorizontal();

		if (Bot == null)
		{
			EditorGUILayout.HelpBox("No active bot", MessageType.Error);
			return;
		}
		if (Bot.RaceAI == null)
		{
			EditorGUILayout.HelpBox("Bot does not have Race AI", MessageType.Error);
			return;
		}

		_scroll = GUILayout.BeginScrollView(_scroll);

		var raceAI = Bot.RaceAI;
		var raceStatus = Bot.RaceStatus;

		GUILayout.Label("AI controls:");
		GUILayout.BeginVertical("Box");
		GUILayout.BeginHorizontal();
		GUI.enabled = raceAI.OverrideThrottle;
		raceAI.NextDesiredThrottle = (double) EditorGUILayout.Slider("Desired throttle", (float) raceAI.NextDesiredThrottle, 0, 1);
		GUI.enabled = true;
		raceAI.OverrideThrottle = GUILayout.Toggle(raceAI.OverrideThrottle, "Ovr", GUILayout.Width(40));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUI.enabled = raceAI.OverrideVelocity;
		raceAI.NextDesiredVelocity = (double) EditorGUILayout.Slider("Desired velocity", (float) raceAI.NextDesiredVelocity, 0, 1);
		GUI.enabled = true;
		raceAI.OverrideVelocity = GUILayout.Toggle(raceAI.OverrideVelocity, "Ovr", GUILayout.Width(40));
		GUILayout.EndHorizontal();
		raceAI.MaxDesiredVelocity = (double) EditorGUILayout.Slider("Max velocity", (float) raceAI.MaxDesiredVelocity, 0, 1);
		GUILayout.BeginHorizontal();
		Track.LaneSwitch lastLaneSwitch = raceAI.AITickData.Count > 0 ? raceAI.AITickData[raceAI.AITickData.Count - 1].desiredLaneSwitch : raceAI.NextDesiredLaneSwitch;
		GUI.enabled = raceAI.OverrideLaneSwitch;
		GUI.changed = false;
		bool switchLeft = GUILayout.Toggle(lastLaneSwitch== Track.LaneSwitch.Left, "Left", "Button");
		if (GUI.changed && switchLeft)
			raceAI.NextDesiredLaneSwitch = Track.LaneSwitch.Left;
		GUI.changed = false;
		bool switchRight = GUILayout.Toggle(lastLaneSwitch == Track.LaneSwitch.Right, "Right", "Button");
		if (GUI.changed && switchRight)
			raceAI.NextDesiredLaneSwitch = Track.LaneSwitch.Right;
		GUI.enabled = true;
		raceAI.OverrideLaneSwitch = GUILayout.Toggle(raceAI.OverrideLaneSwitch, "Ovr", GUILayout.Width(40));
		GUILayout.EndHorizontal();
		GUILayout.BeginHorizontal();
		GUI.enabled = raceAI.OverrideTurbo && raceAI.TurboAvailable;
		GUI.changed = false;
		bool turbo = GUILayout.Toggle(raceAI.TurboActive, "Turbo", "Button");
		if (GUI.changed && turbo)
			raceAI.NextDesiredTurbo = true;
		GUI.enabled = true;
		raceAI.OverrideTurbo = GUILayout.Toggle(raceAI.OverrideTurbo, "Ovr", GUILayout.Width(40));
		GUILayout.EndHorizontal();
		GUILayout.EndVertical();

		GUILayout.Label("Heuristics:");
		GUILayout.BeginVertical("Box");
		raceAI.ThrottleAggressiveness = (double) EditorGUILayout.Slider("Throttle aggressiveness", (float) raceAI.ThrottleAggressiveness, 0, 100);
		raceAI.SlipAngleResponse = (double) EditorGUILayout.Slider("Slip angle response", (float) raceAI.SlipAngleResponse, 0, 5);
		raceAI.TrackHistoryResponse = (double) EditorGUILayout.Slider("Track history response", (float) raceAI.TrackHistoryResponse, 0, 5);
		GUILayout.BeginVertical("Box");
		GUILayout.Label("Look ahead");
		raceAI.LookAheadTime = (double) EditorGUILayout.Slider("Ticks", (float) raceAI.LookAheadTime, 0, 300);
		raceAI.LookAheadDamping = (double) EditorGUILayout.Slider("Damping", (float) raceAI.LookAheadDamping, 0, 5);
		GUILayout.BeginVertical("Box");
		raceAI.LookAheadTorqueWeight = (double) EditorGUILayout.Slider("Torque weight", (float) raceAI.LookAheadTorqueWeight, 0, 20);
		GUILayout.EndVertical();
		raceAI.LookAheadResponse = (double) EditorGUILayout.Slider("Response", (float) raceAI.LookAheadResponse, 0, 5);
		GUILayout.EndVertical();
		GUILayout.EndVertical();

		GUILayout.Label("Car limits:");
		GUILayout.BeginVertical("Box");
		raceAI.TopSpeed = EditorGUILayout.Slider("Top speed", (float) raceAI.TopSpeed, 0, 50);
		raceAI.AccelerationStrength = EditorGUILayout.Slider("Acceleration strength", (float) raceAI.AccelerationStrength, 0, 0.1f);
		raceAI.MaxSlipAngle = EditorGUILayout.Slider("Max slip angle", (float) raceAI.MaxSlipAngle, 0, 90);
		GUILayout.EndVertical();

		GUILayout.EndScrollView();
	}

	void Update()
	{
		Repaint();
	}
}